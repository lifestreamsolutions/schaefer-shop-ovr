﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager instance;

    public string File_Prefix = "LocalizationData" + Path.DirectorySeparatorChar + "LocalizedText_";

    public delegate void LocalizationUpdate();
    public static LocalizationUpdate OnLocalizationUpdate;

    public List<string> languageList = new List<string>();

    Dictionary<string, string> _localizedText;
    bool _isReady = false;
    string _missingTextString = "Localized text not found";

    public int SelectedLanguageIndex
    {
        get { return PlayerPrefs.GetInt("SelectedLanguageIndex", 0); }
        set
        {
            if (!PlayerPrefs.HasKey("SelectedLanguageIndex"))
            {
                PlayerPrefs.GetInt("SelectedLanguageIndex", 0);
            }
            PlayerPrefs.SetInt("SelectedLanguageIndex", value);
        }
    }

    /*string DeviceLanguage
    {
        return (Application.systemLanguage =="English")?"EN":"DE";
    }*/

    string SelectedLanguage
    {
        get { return PlayerPrefs.GetString("SelectedLanguage", "EN"); }
        set
        {
            if (!PlayerPrefs.HasKey("SelectedLanguage"))
            {
                PlayerPrefs.GetString("SelectedLanguage", "EN");
            }
            PlayerPrefs.SetString("SelectedLanguage", value);
        }
    }

    // Use this for initialization
    void Awake()
    {
        if (!PlayerPrefs.HasKey("SelectedLanguage"))
        {
            if (Application.systemLanguage.Equals(SystemLanguage.German))
            {
                SelectedLanguage = "DE";
                //                PlayerPrefs.GetString("SelectedLanguage", "DE");
                SelectedLanguageIndex = 1;
            }
            else
            {
                SelectedLanguage = "EN";
                //                PlayerPrefs.GetString("SelectedLanguage", "EN");
                SelectedLanguageIndex = 0;
            }
        }

        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        ChangeLanguage(SelectedLanguageIndex);
        //        LoadLocalizedText(File_Prefix + SelectedLanguage + ".json");
    }

    public void LoadLocalizedText(string fileName)
    {
        _localizedText = new Dictionary<string, string>();
        //        string filePath = Path.Combine(@Application.streamingAssetsPath, fileName);

        //        if (File.Exists(filePath))
        {
            string dataAsJson = Resources.Load<TextAsset>(fileName).text;// File.ReadAllText(filePath);
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                _localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            }

            Debug.Log("Data loaded, dictionary contains: " + _localizedText.Count + " entries");
        }
        /*else
        {
            Debug.LogError("Cannot find file!");
        }*/

        _isReady = true;

        if (OnLocalizationUpdate != null)
            OnLocalizationUpdate();
    }

    public string GetLocalizedValue(string key)
    {
        string result = string.Empty;// missingTextString;
        if (_localizedText.ContainsKey(key))
        {
            result = _localizedText[key];
        }

        /*if (SelectedLanguage.EndsWith("ARA"))
        {
            result = ArabicSupport.ArabicFixer.Fix(result);
        }*/

        return result;
    }

    public bool GetIsReady()
    {
        return _isReady;
    }

    public void ChangeLanguage(int langIndex)
    {
        string lang = languageList[langIndex];
        SelectedLanguageIndex = langIndex;
        SelectedLanguage = lang;
        LoadLocalizedText(File_Prefix + SelectedLanguage /*+ ".txt"*/);
    }

    public void ToggleLanguage()
    {
        int langIndex = 0;//SelectedLanguageIndex == 0 ? 1 : 0;
        if (SelectedLanguageIndex == languageList.Count - 1)
        {
            SelectedLanguageIndex = 0;
        }
        else
        {
            SelectedLanguageIndex++;
        }
        langIndex = SelectedLanguageIndex;
        string lang = languageList[langIndex];
        SelectedLanguageIndex = langIndex;
        SelectedLanguage = lang;
        LoadLocalizedText(File_Prefix + SelectedLanguage /*+ ".txt"*/);
    }
}