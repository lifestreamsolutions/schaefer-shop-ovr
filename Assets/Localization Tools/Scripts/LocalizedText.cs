﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    public string key;

    void Awake()
    {
        if (string.IsNullOrEmpty(key))
        {
            key = GetComponent<Text>().text;
        }
    }

    void OnEnable()
    {
        LocalizationManager.OnLocalizationUpdate += Localize;
        Localize();
    }

    /*// Use this for initialization
    void Start()
    {
        Localize();
    }*/

    void OnDisable()
    {
        LocalizationManager.OnLocalizationUpdate -= Localize;
    }

    void Localize()
    {
        Text text = GetComponent<Text>();
        string locText = LocalizationManager.instance.GetLocalizedValue(key);
//                print(locText);
        if (!string.IsNullOrEmpty(locText))
            text.text = locText;
    }
}