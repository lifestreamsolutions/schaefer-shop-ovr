﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProjectDetailsItem : MonoBehaviour
{
    #region Public_Variables
    public int parentProjectIndex;
    public Text indexText;
    public Image iconImage;
    public Text titleText;
    public Text descriptionText;
    public Button viewButton;
    public Button deleteButton;
    public Button updateButton;

    public string ProjectID
    {
        get { return _projectID; }
    }

    public int VariantsCount
    {
        get { return _variantsCount; }
    }
    #endregion

    #region Private_Variables
    string _projectID = "";
    //    float _versionNumber;
    int _variantsCount;

    private bool _isUpdateAvailable
    {
        get
        {
            if (string.IsNullOrEmpty(_projectID))
                return false;
            if (_variantsCount > 0)
                return AppManager.instance.IsUpdateAvailableForProject(_projectID);
            else
                return AppManager.instance.IsUpdateAvailableForSubProject(_projectID);
        }
    }
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake() { }

    void OnEnable()
    {
        CheckForUpdate();
        EventManager.ProjectUpdateAvailable += CheckForUpdate;
    }

    // Use this for initialization
    void Start() { }

    void OnDisable()
    {
        transform.ResetTransfrom();
        EventManager.ProjectUpdateAvailable -= CheckForUpdate;
    }
    #endregion

    #region Private_Methods

    void CheckForUpdate()
    {
        ProjectsPanel projectsPanel = GetComponentInParent<ProjectsPanel>();
        if (projectsPanel && projectsPanel.projectType.Equals(ProjectType.Normal))
            updateButton.gameObject.SetActive(_isUpdateAvailable);
        else
            updateButton.gameObject.SetActive(false);
    }

    #endregion

    #region Public_Methods
    public void SetDetails(ProjectDetailsTemplate projectDetails, int projectListIndex = 0)
    {
        indexText.text = (transform.GetSiblingIndex()+1).ToString();
        parentProjectIndex = projectDetails.parentProjectIndex;
        titleText.text = projectDetails.name;
        descriptionText.text = projectDetails.description;
        if (projectDetails.iconImage)
        {
            iconImage.sprite = Sprite.Create(projectDetails.iconImage,
                  new Rect(0, 0, projectDetails.iconImage.width, projectDetails.iconImage.height), Vector2.zero);
        }
        _projectID = projectDetails.projectID;
        //        _versionNumber = projectDetails.currentProjectVersion;
        if (projectDetails.variants != null)
            _variantsCount = projectDetails.variants.Count;
        gameObject.name = _projectID;
        CheckForUpdate();
    }

    public void SetViewEvent(Action action)
    {
        viewButton.onClick.RemoveAllListeners();
        viewButton.onClick.AddListener(() =>
        {
            if (action != null) action.Invoke();
        });
    }

    public void SetUpdateEvent(Action action)
    {
        updateButton.onClick.RemoveAllListeners();
        updateButton.onClick.AddListener(() =>
        {
            if (action != null) action.Invoke();
        });
    }

    public void UpdateButtonClicked()
    {
        if (_variantsCount > 0)
            AppManager.instance.UpdateProject(_projectID);
        else
            AppManager.instance.UpdateSubProject(_projectID);
        StopCoroutine("CheckForUpdateStatusEnumerator");
        StartCoroutine("CheckForUpdateStatusEnumerator");
    }

    public void SetDeleteEvent(Action action)
    {
        if (_variantsCount > 0)
        {
            deleteButton.gameObject.SetActive(true);
            deleteButton.onClick.RemoveAllListeners();
            deleteButton.onClick.AddListener(() =>
            {
                if (action != null) action.Invoke();
            });
        }
    }
    #endregion

    #region Coroutines
    IEnumerator CheckForUpdateStatusEnumerator()
    {
        while (gameObject.activeInHierarchy)
        {
            yield return new WaitForSeconds(1);
            CheckForUpdate();
            if (!updateButton.gameObject.activeInHierarchy)
                yield break;
        }
    }
    #endregion

    #region Custom_CallBacks
    #endregion
}