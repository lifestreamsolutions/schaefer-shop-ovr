﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BarcodeScanPanel : MonoBehaviour
{
    #region Public_Variables
    public RawImage rawImage;
    #endregion

    #region Private_Variables
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake() { }

    void OnEnable()
    {
#if UNITY_ANDROID
        if (UniAndroidPermission.IsPermitted(AndroidPermission.CAMERA))
        { EnableCamera(); }
        else
        {
            UniAndroidPermission.RequestPremission(AndroidPermission.CAMERA, () =>
            {
                Debug.Log("Camera Permission Enabled Now.");// not getting this callback so manually check permission in coroutine
            });
            StartCoroutine(CheckCameraPermission());
        }
#else
                EnableCamera();
#endif
    }

    // Use this for initialization
    void Start() { }

    void OnDisable()
    {
        BarcodeScannerController.OnBarcodeScanned -= OnBarcodeScanned;
        BarcodeScannerController.instance.StopScanning();
    }
    #endregion

    #region Private_Methods

    void EnableCamera()
    {
        BarcodeScannerController.instance.SetTargetRawImage(rawImage);
        BarcodeScannerController.instance.StartScanning();
        BarcodeScannerController.OnBarcodeScanned += OnBarcodeScanned;
    }
    #endregion

    #region Public_Methods

    public void Back()
    {
        UIManager.instance.Back();
    }
    #endregion

    #region Coroutines

#if UNITY_ANDROID
    IEnumerator CheckCameraPermission()
    {
        yield return new WaitForSeconds(0.1f);
        if (UniAndroidPermission.IsPermitted(AndroidPermission.CAMERA))
        {
            Debug.Log("Camera Permission");
            EnableCamera();
        }
        else
        {
            Back();
        }
    }
#endif
#endregion

    #region Custom_CallBacks
    private void OnBarcodeScanned(string barcodescannervalue)
    {
        AppManager.instance.CurrentProjectID_Download = barcodescannervalue;
        Back();
    }
    #endregion
}
