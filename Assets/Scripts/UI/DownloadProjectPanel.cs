﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;

public class DownloadProjectPanel : MonoBehaviour
{
    #region Public_Variables
    public InputField projectIdInputField;
    public InputField pinInputField;
    #endregion

    #region Private_Variables
    //ref. i/p field where input from vrtul kbrd will be set
    InputField _targetInputField;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake() { }

    void OnEnable()
    {
        projectIdInputField.text = AppManager.instance.CurrentProjectID_Download;
        //        pinInputField.text = AppManager.instance.PIN;
#if OVR
        VirtualKeyboardController.OnKeyBoardTextChanged += OnKeyBoardTextChanged;
#endif
        ProjectIDInputFieldLostFocus();
    }

    // Use this for initialization
    void Start() { }

    void OnDisable()
    {
        _targetInputField = null;
        UIManager.instance.ToggleKeyboardCanvas(false);
        MessagePopupPanel.instance.HideMessageBox();
#if OVR
        VirtualKeyboardController.OnKeyBoardTextChanged -= OnKeyBoardTextChanged;
#endif
    }

    #endregion

    #region Private_Methods

    void DownloadProject()
    {
        AppManager.instance.DownloadProject();
    }

    #endregion

    #region Public_Methods
    public void Back()
    {
        UIManager.instance.Back();
    }

    public void ScanClicked()
    {
#if UNITY_ANDROID
        if (UniAndroidPermission.IsPermitted(AndroidPermission.CAMERA))
        {
            UIManager.instance.ShowBarcodeScanPanel();
        }
        else
        {
            UniAndroidPermission.RequestPremission(AndroidPermission.CAMERA, () =>
            {
                Debug.Log("Camera Permission Enabled Now.");// not getting this callback so manually check permission in coroutine
            });
            StartCoroutine(CheckCameraPermission());
        }
#elif UNITY_IOS
        UIManager.instance.ShowBarcodeScanPanel();
#endif
    }

    public void DownloadClicked()
    {
        string projectIdFromInput = projectIdInputField.text.Trim();
        string pinFromInput = pinInputField.text.Trim();
        string dl = LocalizationManager.instance.GetLocalizedValue("Download");
        string validPinMsg = LocalizationManager.instance.GetLocalizedValue("Valid PIN Message");
        string validProjIdMsg = LocalizationManager.instance.GetLocalizedValue("Valid Poject-ID Message");

        if (AppManager.instance.IsProjectDownloaded(projectIdFromInput))
        {
            string msg = LocalizationManager.instance.GetLocalizedValue("Already Project Downloaded Message");
            MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, dl, msg, null, null, Constants.Ok);
            return;
        }

        UIManager.instance.ToggleKeyboardCanvas(false);

        if (!string.IsNullOrEmpty(projectIdFromInput) && (!string.IsNullOrEmpty(pinFromInput)
            || !pinInputField.gameObject.activeInHierarchy))
        {
            string yes = LocalizationManager.instance.GetLocalizedValue(Constants.Yes);
            string no = LocalizationManager.instance.GetLocalizedValue(Constants.No);
            string confirmMsg = LocalizationManager.instance.GetLocalizedValue("Confirm Message");
            MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Confirm, dl, confirmMsg,
                () =>
                {
                    AppManager.instance.CurrentProjectID_Download = projectIdFromInput;
                    AppManager.instance.Download_PIN = pinFromInput;
                    DownloadProject();
                }, null, yes, no);
        }
        else
        {
            if (string.IsNullOrEmpty(projectIdFromInput))
            {
                MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, dl, validProjIdMsg,
                    null, null, Constants.Ok);
            }
            else if (string.IsNullOrEmpty(pinFromInput) && pinInputField.gameObject.activeInHierarchy)
            {
                MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, dl, validPinMsg,
                    null, null, Constants.Ok);
            }
        }
    }

    public void SelectProjectIDInputField(bool enable)
    {
#if OVR
        _targetInputField = projectIdInputField;
        VirtualKeyboardController.instance.SetDefaultString(projectIdInputField.text);
#endif
        UIManager.instance.ToggleKeyboardCanvas(enable);
    }

    public void SelectPINInputField(bool enable)
    {
#if OVR
        _targetInputField = pinInputField;
        VirtualKeyboardController.instance.SetDefaultString(pinInputField.text);
#endif
        UIManager.instance.ToggleKeyboardCanvas(enable);
    }

    public void ProjectIDInputFieldLostFocus()
    {
        string projectIdFromInput = projectIdInputField.text.Trim();
        if (!string.IsNullOrEmpty(projectIdFromInput))
        {
            StartCoroutine(CheckIfPINRequired(projectIdFromInput));
        }
    }
    #endregion

    #region Coroutines
#if UNITY_ANDROID
    IEnumerator CheckCameraPermission()
    {
        yield return new WaitForSeconds(0.1f);
        if (UniAndroidPermission.IsPermitted(AndroidPermission.CAMERA))
        {
            Debug.Log("Camera Permission");
            UIManager.instance.ShowBarcodeScanPanel();
        }
    }
#endif

    IEnumerator CheckIfPINRequired(string projectIdFromInput)
    {
        Debug.Log("Checking If PIN Required");

        WWWForm dataForm = new WWWForm();
        dataForm.AddField("barcode", projectIdFromInput);

        WWW pinRequired = new WWW(Constants.PIN_Required_URL, dataForm);
        yield return pinRequired;

        if (pinRequired.error == null)
        {
            JObject jo = JObject.Parse(pinRequired.text);
            if (jo["isPinRequired"] != null)
            {
                bool required = (bool)jo["isPinRequired"];
                pinInputField.gameObject.SetActive(required);
                Debug.Log("PIN Required");
            }
            else
            {
                pinInputField.gameObject.SetActive(true);
            }
        }
    }

    #endregion

    #region Custom_CallBacks

    void OnKeyBoardTextChanged(string text)
    {
        if (_targetInputField)
            _targetInputField.text = text;
    }
    #endregion
}
