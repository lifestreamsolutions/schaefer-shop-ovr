﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MessagePopupPanel : MonoBehaviour
{
    public static MessagePopupPanel instance;

    #region Public_Variables
    public GameObject messageBox;
    public Text messageTitleText;
    public Text messageDescriptionText;

    public Text okButtonText;
    public Text cancelButtonText;
    #endregion

    #region Private_Variables
    private Action _messageOkAction;
    private Action _messageCancelAction;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
    }

    // Use this for initialization
    void Start()
    {
    }

    void OnDisable()
    {
    }
    #endregion

    #region Private_Methods

    void ToggleMessageBox(bool enable)
    {
        if (messageBox)
            messageBox.SetActive(enable);
    }
    #endregion

    #region Public_Methods

    public void OnOkButtonClick()
    {
        if (_messageOkAction != null)
            _messageOkAction.Invoke();
        _messageOkAction = null;
        ToggleMessageBox(false);
    }

    public void OnCancelButtonClick()
    {
        if (_messageCancelAction != null)
            _messageCancelAction.Invoke();
        _messageCancelAction = null;
        ToggleMessageBox(false);
    }

    public void ShowMessageBox(MessagePopupType messageType, string title, string description, Action okCallback, Action cancelCallback,
        string okButtonName = null, string cancelButtonName = null)
    {
        Debug.Log("[ShowMessageBox] : " + messageType);

        cancelButtonText.transform.parent.gameObject.SetActive(messageType.Equals(MessagePopupType.Confirm));

        messageTitleText.text = title;
        messageDescriptionText.text = description;
        _messageOkAction = okCallback;
        _messageCancelAction = cancelCallback;

        if (!string.IsNullOrEmpty(okButtonName))
            okButtonText.text = okButtonName;

        if (!string.IsNullOrEmpty(cancelButtonName))
            cancelButtonText.text = cancelButtonName;

        ToggleMessageBox(true);
    }

    public void HideMessageBox()
    {
        ToggleMessageBox(false);
    }

    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    #endregion
}

public enum MessagePopupType
{
    Simple,
    Confirm
}