﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using ZXing;

public class HomePanel : MonoBehaviour
{
    #region Public_Variables
    public Image selectedLangImage;
    public List<Sprite> langImages;
    public GameObject languageSelectionPanel;
    public List<Toggle> languageToggles;
    #endregion

    #region Private_Variables
    int _currentSelectedLanguage;
    int _intitialSelectedLanguage;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake() { }

    void OnEnable()
    {
        selectedLangImage.sprite = langImages[LocalizationManager.instance.SelectedLanguageIndex];
    }

    // Use this for initialization
    void Start()
    {
    }

    void OnDisable() { }
    #endregion

    #region Private_Methods
    #endregion

    #region Public_Methods

    public void DownloadProjectClicked()
    {
        UIManager.instance.ShowDownloadProjectPanel();
    }

    public void DemoProjectsClicked()
    {
        UIManager.instance.ShowDemoProjectsPanel();
    }

    public void OpenProjectsClicked()
    {
        UIManager.instance.ShowProjectsPanel();
    }

    public void OnInfoButtonClick()
    {
        UIManager.instance.ToggleInfoPanel(true);
    }

    public void ChangeLanguage()
    {
        LocalizationManager.instance.ToggleLanguage();
        selectedLangImage.sprite = langImages[LocalizationManager.instance.SelectedLanguageIndex];
    }

    public void OpenSchaferWebpage()
    {
        Application.OpenURL(AppManager.instance.WebPage_URL);
    }

    public void OnLanguageSelectionButtonClick()
    {
        foreach (var toggle in languageToggles)
        {
            toggle.isOn = false;
        }
        _intitialSelectedLanguage = LocalizationManager.instance.SelectedLanguageIndex;
        _currentSelectedLanguage = _intitialSelectedLanguage;
        languageToggles[_currentSelectedLanguage].isOn = true;

        UIManager.instance.overrideSystemEscapeActionForUI = true;
        AppManager.OnSystemEscapePressed += OnSystemEscapePressed;

        languageSelectionPanel.SetActive(true);
    }

    public void OkAtLanguageSelection()
    {
        UIManager.instance.overrideSystemEscapeActionForUI = false;
        LocalizationManager.instance.ChangeLanguage(_currentSelectedLanguage);
        languageSelectionPanel.SetActive(false);
    }

    public void CancelAtLanguageSelection()
    {
        LocalizationManager.instance.ChangeLanguage(_intitialSelectedLanguage);
        UIManager.instance.overrideSystemEscapeActionForUI = false;
        languageSelectionPanel.SetActive(false);
        AppManager.OnSystemEscapePressed -= OnSystemEscapePressed;
    }

    public void LanguageSelect(int index)
    {
        _currentSelectedLanguage = index;
        LocalizationManager.instance.ChangeLanguage(_currentSelectedLanguage);
    }
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks

    void OnSystemEscapePressed()
    {
        CancelAtLanguageSelection();
    }
    #endregion
}
