﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPanel : MonoBehaviour
{
    public static LoadingPanel instance;

    #region Public_Variables
    public GameObject loadingPanel;
    public Text messageText;

    public Transform loadingIcon;

    public GameObject cancelButton;
    #endregion

    #region Private_Variables
    private Action _loadingCancelAction;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake()
    {
        instance = this;
        loadingPanel.SetActive(false);
    }

    void OnEnable()
    {
    }

    // Use this for initialization
    void Start()
    {
    }

    void OnDisable()
    {
        cancelButton.SetActive(false);
    }
    #endregion

    #region Private_Methods
    #endregion

    #region Public_Methods

    public void ShowLoadingPanel(string message, Action cancelAction)
    {
        _loadingCancelAction = cancelAction;
        cancelButton.SetActive(_loadingCancelAction != null);
        Debug.Log("[ShowLoadingPanel]");
        messageText.text = message;
        loadingPanel.SetActive(true);
        StartCoroutine(LoadingIconAnim());
    }

    public void HideLoadingPanel()
    {
        Debug.Log("[HideLoadingPanel]");
        loadingPanel.SetActive(false);
    }

    public void CancelClicked()
    {
        if (_loadingCancelAction != null)
        {
            _loadingCancelAction.Invoke();
            _loadingCancelAction = null;
            HideLoadingPanel();
        }
    }
    #endregion

    #region Coroutines

    IEnumerator LoadingIconAnim()
    {
        while (loadingPanel.activeInHierarchy)
        {
            loadingIcon.localEulerAngles -= Vector3.forward * 30;
            yield return new WaitForSeconds(0.0833f);
        }
    }
    #endregion

    #region Custom_CallBacks
    #endregion
}
