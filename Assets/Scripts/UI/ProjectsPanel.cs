﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;

public class ProjectsPanel : MonoBehaviour
{
    #region Public_Variables
    public ProjectType projectType;
    public Text titleText;
    public GameObject projectDetailsItemPrefab;
    public Transform content;

    public float spawnTimeDelay = 0.1f;

    public float scrollRectValue;

    public GameObject pinPanel;
    public InputField pinInputField;
    #endregion

    #region Private_Variables
    string _titleTextAtFirst;
    bool _showingVariants = false;
    int _variantsLevel = 0;
    AlmostAStack<int> _varientHierarchyIndices = new AlmostAStack<int>();

    List<ProjectDetailsTemplate> Projects
    {
        get
        {
            if (projectType.Equals(ProjectType.Demo))
                return DataManager.instance.demoProjects;
            else
                return DataManager.instance.projects;
        }
    }

    ProjectDetailsItem _itemGoingToUpdate;
    bool _askedForProjectDelete;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks

    void Awake()
    {
        _titleTextAtFirst = titleText.text;
    }

    void OnEnable()
    {
        if (_variantsLevel == 0)
            titleText.text = LocalizationManager.instance.GetLocalizedValue(_titleTextAtFirst);

        UIManager.instance.overrideSystemEscapeActionForUI = true;
        AppManager.OnSystemEscapePressed += Back;
        if (Projects.Count == 0)
        {
            string prjs = LocalizationManager.instance.GetLocalizedValue("Projects");
            string noPrjsMessage = LocalizationManager.instance.GetLocalizedValue("No Projects Message");
            noPrjsMessage = noPrjsMessage.Replace("\\n", "\n");
            MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, prjs, noPrjsMessage, Back, null, Constants.Ok);
            return;
        }
        if (content.childCount < Projects.Count && _variantsLevel == 0)
        {
            ShowTopHierarchicalProjects();
        }
        VirtualKeyboardController.OnKeyBoardTextChanged += OnKeyBoardTextChanged;
        pinInputField.text = AppManager.instance.Update_PIN;
    }

    // Use this for initialization
    void Start()
    {
        ShowTopHierarchicalProjects();
    }

    void OnDisable()
    {
        UIManager.instance.overrideSystemEscapeActionForUI = false;
        AppManager.OnSystemEscapePressed -= Back;
        if (Projects.Count == 0)
        {
            MessagePopupPanel.instance.HideMessageBox();
        }
        TogglePinPanel(false);
        VirtualKeyboardController.OnKeyBoardTextChanged -= OnKeyBoardTextChanged;
        _askedForProjectDelete = false;
    }
    #endregion

    #region Private_Methods
    void ClearProjects()
    {
        content.ClearAllChilds();
    }

    GameObject InstantiateProject()
    {
        GameObject demoProj = Instantiate(projectDetailsItemPrefab);
        demoProj.transform.SetParent(content);
        demoProj.transform.ResetTransfrom();
        return demoProj;
    }

    void ShowProjectItem(ProjectDetailsTemplate projectDetails)
    {
        GameObject temp = InstantiateProject();
        ProjectDetailsItem item = temp.GetComponent<ProjectDetailsItem>();
        item.SetDetails(projectDetails);
        item.SetViewEvent(() => { OnProjectViewClicked(projectDetails); });
        item.SetUpdateEvent(() => { OnProjectUpdateClicked(item); });
        if (projectType.Equals(ProjectType.Normal))
            item.SetDeleteEvent(() => { OnProjectDeleteClicked(item, temp); });
        item.GetComponent<Animation>().Play();
    }

    void ShowTopHierarchicalProjects()
    {
        _varientHierarchyIndices.items.Clear();
        _varientHierarchyIndices.items.Add(0);
        ShowProjectVariants(Projects);
    }

    void ShowProjectVariants(List<ProjectDetailsTemplate> projectVariants)
    {
        StopCoroutine("ShowProjectsEnumerator");
        ClearProjects();
        StartCoroutine("ShowProjectsEnumerator", projectVariants);
    }

    void BackToParentProject()
    {
        List<ProjectDetailsTemplate> projects = Projects;
        for (int i = 0; i < _variantsLevel; i++)
        {
            int index = _varientHierarchyIndices.Pop();
            titleText.text = projects[index].name;
            projects = projects[index].variants;
        }
        ShowProjectVariants(projects);
    }

    #endregion

    #region Public_Methods
    public void Back()
    {
        if (AppManager.instance.projectOpened || _askedForProjectDelete)
            return;

        if (pinPanel.activeInHierarchy)
        {
            TogglePinPanel(false);
            return;
        }

        if (_showingVariants && _variantsLevel > 0)
        {
            _variantsLevel--;
            BackToParentProject();
            if (_variantsLevel == 0)
            {
                //                titleText.text = _titleTextAtFirst;
                titleText.text = LocalizationManager.instance.GetLocalizedValue(_titleTextAtFirst);
            }
        }
        else
        {
            _showingVariants = false;
            UIManager.instance.Back();
        }
    }

    public void OnProjectViewClicked(ProjectDetailsTemplate projectDetails)
    {
        if (projectDetails.variants != null && projectDetails.variants.Count > 0)
        {
            _showingVariants = true;
            _variantsLevel++;
            titleText.text = projectDetails.name;
            _varientHierarchyIndices.items.Add(projectDetails.parentProjectIndex);
            ShowProjectVariants(projectDetails.variants);
        }
        else
        {
            AppManager.instance.SetSubProjectHierarchy(projectDetails.projectID, projectType);
            AppManager.instance.ProjectSelected(AppManager.instance.CurrentSubProjectID_Selected, projectType);
        }
    }

    public void OnProjectUpdateClicked(ProjectDetailsItem projectDetailsItem)
    {
        _itemGoingToUpdate = projectDetailsItem;
        TogglePinPanel(true);
        if (projectDetailsItem.VariantsCount > 0)
        {
            StartCoroutine(CheckIfPINRequired(projectDetailsItem.ProjectID));
        }
        else
        {
            StartCoroutine(CheckIfPINRequired(AppManager.instance.GetRootProjectID(projectDetailsItem.ProjectID)));
        }
    }

    public void UpdateProject()
    {
        string pinFromInput = pinInputField.text.Trim();

        if (string.IsNullOrEmpty(pinFromInput) && pinInputField.gameObject.activeInHierarchy)
        {
            string udt = LocalizationManager.instance.GetLocalizedValue("Update");
            string validPinMsg = LocalizationManager.instance.GetLocalizedValue("Valid PIN Message");
            MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, udt, validPinMsg,
                null, null, Constants.Ok);
        }
        else
        {
            AppManager.instance.Update_PIN = pinFromInput;
            TogglePinPanel(false);
            _itemGoingToUpdate.UpdateButtonClicked();
        }
    }

    public void TogglePinPanel(bool enable)
    {
        pinInputField.gameObject.SetActive(true);
        pinPanel.SetActive(enable);
#if OVR
        VirtualKeyboardController.instance.ClearKeyboardTextString();
        UIManager.instance.ToggleKeyboardCanvas(enable);
#endif
    }

    public void OnScroll(Vector2 value)
    {
        scrollRectValue = value.y;
    }

    public void OnProjectDeleteClicked(ProjectDetailsItem projectDetailsItem, GameObject go)
    {
        if (projectType.Equals(ProjectType.Demo))
            return;

        _askedForProjectDelete = true;
        string yes = LocalizationManager.instance.GetLocalizedValue(Constants.Yes);
        string no = LocalizationManager.instance.GetLocalizedValue(Constants.No);
        string quit = LocalizationManager.instance.GetLocalizedValue("Delete Project");
        string confirmMsg = LocalizationManager.instance.GetLocalizedValue("Confirm Message");
        MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Confirm, quit, confirmMsg,
            () =>
            {
                DataManager.instance.DeleteProject(projectDetailsItem.ProjectID);
                //                go.SetActive(false);
                _askedForProjectDelete = false;
                ShowTopHierarchicalProjects();
            }, () =>
            {
                _askedForProjectDelete = false;
            }, yes, no);
    }
    #endregion

    #region Coroutines

    IEnumerator ShowProjectsEnumerator(List<ProjectDetailsTemplate> projectVariants)
    {
        yield return new WaitForSeconds(0.15f);
        scrollRectValue = 0;
        List<ProjectDetailsTemplate> projects = projectVariants;
        foreach (var project in projects)
        {
            ShowProjectItem(project);
            yield return new WaitForSeconds(spawnTimeDelay);
            while (scrollRectValue > 0)
            {
                yield return null;
            }
        }
    }

    IEnumerator CheckIfPINRequired(string projectIdFromInput)
    {
        Debug.Log("Checking If PIN Required");

        WWWForm dataForm = new WWWForm();
        dataForm.AddField("barcode", projectIdFromInput);

        WWW pinRequired = new WWW(Constants.PIN_Required_URL, dataForm);
        yield return pinRequired;

        if (pinRequired.error == null)
        {
            JObject jo = JObject.Parse(pinRequired.text);
            if (jo["isPinRequired"] != null)
            {
                bool required = (bool)jo["isPinRequired"];
                pinInputField.gameObject.SetActive(required);
                Debug.Log("PIN Required");
            }
            else
            {
                pinInputField.gameObject.SetActive(true);
            }
        }
    }
    #endregion

    #region Custom_CallBacks
    void OnKeyBoardTextChanged(string text)
    {
        pinInputField.text = text;
    }
    #endregion
}