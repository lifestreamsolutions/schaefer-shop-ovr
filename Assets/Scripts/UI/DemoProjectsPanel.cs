﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DemoProjectsPanel : MonoBehaviour
{
    #region Public_Variables
    public GameObject projectDetailsItemPrefab;
    public Transform content;
    #endregion

    #region Private_Variables
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake() { }

    void OnEnable()
    {
        ClearDemoProjects();
        ShowDemoProjects();
    }
    // Use this for initialization
    void Start() { }
    void OnDisable() { }
    #endregion

    #region Private_Methods

    void ClearDemoProjects()
    {
        content.ClearAllChilds();
    }

    GameObject InstantiateDemoProject()
    {
        GameObject demoProj = Instantiate(projectDetailsItemPrefab);
        demoProj.transform.SetParent(content);
        demoProj.transform.ResetTransfrom();
        return demoProj;
    }

    void ShowDemoProjects()
    {
        List<ProjectDetailsTemplate> demoProjects = DataManager.instance.demoProjects;
        foreach (var demoProject in demoProjects)
        {
            GameObject temp = InstantiateDemoProject();
            temp.GetComponent<ProjectDetailsItem>().SetDetails(demoProject);
        }
    }
    #endregion

    #region Public_Methods

    public void Back()
    {
        UIManager.instance.Back();
    }
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    #endregion
}
