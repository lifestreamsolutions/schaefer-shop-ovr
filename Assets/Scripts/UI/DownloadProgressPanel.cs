﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DownloadProgressPanel : MonoBehaviour
{
    #region Public_Variables
    public Slider progressSlider;
    public float lerpSpeed = 1;
    #endregion

    #region Private_Variables
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake() { }

    void OnEnable()
    {
        progressSlider.value = 0;
    }
    // Use this for initialization
    void Start() { }
    // Update is called once per frame
    void Update()
    {
        progressSlider.value = Mathf.Lerp(progressSlider.value, DataManager.instance.downloadProgress,
            Time.deltaTime * lerpSpeed);
    }
    void OnDisable() { }
    #endregion

    #region Private_Methods
    #endregion

    #region Public_Methods
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    #endregion
}
