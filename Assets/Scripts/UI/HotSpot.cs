﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HotSpot : MonoBehaviour
{
    #region Public_Variables
    public Text labelText;
    #endregion

    #region Private_Variables
    private Action _action = null;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake() { }
    void OnEnable() { }
    // Use this for initialization
    void Start() { }
    void OnDisable() { }
    #endregion

    #region Private_Methods
    #endregion

    #region Public_Methods
    public void SetDetails(HotSpotDetailsTemplate hotSpotDetails)
    {
        transform.localEulerAngles = hotSpotDetails.coOrdinates;
        labelText.text = hotSpotDetails.label;
    }

    public void SetOnClickAction(Action action)
    {
        _action = action;
    }

    public void OnClicked()
    {
        Debug.Log("HotSpot Clicked");
        if (_action != null)
            _action.Invoke();
    }
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    #endregion
}
