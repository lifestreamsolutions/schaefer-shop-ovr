﻿using System;
using UnityEngine;
using System.Collections;
using System.Text;

public static class Extensions
{
    #region Public_Methods

    public static void SetLayer(this GameObject gameObject, int layer)
    {
        gameObject.layer = layer;
    }

    public static void SetLayer(this GameObject gameObject, string layer)
    {
        int layerNo = LayerMask.NameToLayer(layer);
        gameObject.layer = layerNo;
    }

    public static void ResetTransfrom(this Transform transform)
    {
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;
    }

    public static void ClearAllChilds(this Transform transform)
    {
        int childs = transform.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }

    public static void DisableAllChilds(this Transform transform)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public static string ToEncodedString(this string dataString)
    {
        byte[] bytesToEncode = Encoding.UTF8.GetBytes(dataString);
        string encodedText = Convert.ToBase64String(bytesToEncode);
        return encodedText;
    }

    public static string ToDecodedString(this string dataString)
    {
        byte[] decodedBytes = Convert.FromBase64String(dataString);
        string decodedText = Encoding.UTF8.GetString(decodedBytes);
        return decodedText;
    }
    #endregion
}
