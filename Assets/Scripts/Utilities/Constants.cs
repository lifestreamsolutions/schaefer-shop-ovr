﻿
using UnityEngine;

public class Constants
{
    #region Public_Variables
    public const string Yes = "Yes";
    public const string No = "No";
    public const string Ok = "Ok";
    public const string Quit = "Quit";

    public const string ProjectDataDetails_Folder_Name = "ProjectDataDetails";
    public const string ProjectData_Folder_Name = "ProjectData";
    public const string Panorama_Folder_Name = "Panoramas";
    public const string DemoProjects_Folder_Name = "DemoProjects";

    public static string SAPath
    {
        get
        {
            if (Application.platform == RuntimePlatform.Android)
                return "jar:file://" + Application.dataPath + "!/assets/";
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
                return "file://" + Application.dataPath + "/Raw/";
            else
                return "file:///" + Application.streamingAssetsPath + "/";
        }
    }

    public const string Schafer_Label = "Schäfer";
    #region URLs
    //    public const string Base_URL = "http://test-schaefer.maldechavda.me/api/";
    public static string Base_URL
    {
        get
        {
            if (AppManager.instance.appMode.Equals(AppMode.Test))
                return AppManager.instance.Test_Base_URL;
            else if (AppManager.instance.appMode.Equals(AppMode.Production))
                return AppManager.instance.Production_Base_URL;
            else
                return string.Empty;
        }
    }
    public static string Download_Project_URL = Base_URL + "project";
    public static string Check_Update_SubProject_URL = Base_URL + "project/current-version";
    public static string Check_Update_Project_URL = Base_URL + "project/barcode/current-version";
    public static string Download_Project_Barcode_URL = Base_URL + "project/barcode";
    public static string PIN_Required_URL = Base_URL + "project/check-pin-required";
    #endregion

    #region KEYs
    public static string CurrentProjectVersion_Key = "currentProjectVersion";
    public static string Success_Key = "success";
    public static string Data_Key = "data";
    public static string Message_Key = "message";
    public static string EN_Message_Key = "en_message";
    public static string DE_Message_Key = "de_message";
    public static string FR_Message_Key = "fr_message";
    #endregion

    #region Extenstions
    public const string PNG_Ext = ".png";
    #endregion

    #region Name
    public const string Map_Image_Name = "map";
    public const string Customer_Logo_Image_Name = "customer_logo";
    public const string Project_Icon_Image_Name = "project_icon";
    public const string HotSpot_Icon_Image_Name = "hot_spot_icon";
    #endregion

    #endregion
}
