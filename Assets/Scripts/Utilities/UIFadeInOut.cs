﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class UIFadeInOut : MonoBehaviour
{
    #region Public_Variables
    public AnimationCurve fadeInCurve;
    public AnimationCurve fadeOutCurve;

    public Color fadeColor = Color.black;
    public Image image;
    #endregion

    #region Private_Variables
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks

    void Start()
    {
        SetColor(fadeColor);
    }
    #endregion

    #region Private_Methods
    #endregion

    #region Public_Methods

    public void SetColor(Color targetColor)
    {
        if (!image)
            image = transform.GetComponentInChildren<Image>();
        if (image)
        {
            fadeColor = targetColor;
            image.color = fadeColor;
        }
    }

    /// <summary>
    /// Alpha 0->1
    /// </summary>
    /// <param name="fadeInTime"></param>
    /// <param name="callbaclk"></param>
    public void StartFadeIn(float fadeInTime, Action callbaclk)
    {
        StartCoroutine(FadeIn(fadeInTime, callbaclk));
    }

    /// <summary>
    /// Alpha 1->0
    /// </summary>
    /// <param name="fadeOutTime"></param>
    /// <param name="callbaclk"></param>
    public void StartFadeOut(float fadeOutTime, Action callbaclk)
    {
        StartCoroutine(FadeOut(fadeOutTime, callbaclk));
    }
    #endregion

    #region Coroutines

    IEnumerator FadeEnumerator(float start, float end, float fadeTime)
    {
        float progress = 0;
        CanvasGroup canvasRenderer = GetComponent<CanvasGroup>();
        canvasRenderer.alpha = (progress);
        while (progress < 1)
        {
            progress += (Time.unscaledDeltaTime / fadeTime);
            if (end > start)
                canvasRenderer.alpha = (fadeInCurve.Evaluate(progress));
            else
            {
                canvasRenderer.alpha = (fadeOutCurve.Evaluate(1 - progress));
            }
            yield return null;
        }
        canvasRenderer.alpha = end;
    }

    IEnumerator FadeIn(float fadeInTime, Action callbaclk)
    {
        yield return StartCoroutine(FadeEnumerator(0, 1, fadeInTime));

        if (callbaclk != null)
        {
            callbaclk.Invoke();
        }
    }

    IEnumerator FadeOut(float fadeOutTime, Action callbaclk)
    {
        yield return StartCoroutine(FadeEnumerator(1, 0, fadeOutTime));

        if (callbaclk != null)
        {
            callbaclk.Invoke();
        }
    }
    #endregion

    #region Custom_CallBacks
    #endregion
}
