﻿using UnityEngine;
using System.IO;
using System.Security.Permissions;
using System.Threading;
using ThreadPriority = System.Threading.ThreadPriority;

public class Utilities
{
    #region Public_Methods
    public static void CreateTextureFromCameraRenderTexture(Camera previewCamera, int width, int height, out Texture2D tex)
    {
        RenderTexture tempRT = new RenderTexture(width, height, 24);
        tempRT.antiAliasing = 4;
        previewCamera.targetTexture = tempRT;
        previewCamera.Render();
        RenderTexture.active = tempRT;

        // Create a new Texture2D and read the RenderTexture image into it
        tex = new Texture2D(tempRT.width, tempRT.height, TextureFormat.ARGB32, false);
        tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
        tex.Apply();
        RenderTexture.active = null; //can help avoid errors 
    }

    public static void SaveTextureAsPNG(Texture2D tex, string filePath)
    {
        byte[] bytes;
        bytes = tex.EncodeToPNG();
        File.WriteAllBytes(filePath, bytes);
        /*Thread t = new Thread(() =>
        {
            File.WriteAllBytes(filePath, bytes);
        });
        t.IsBackground = true;
        t.Start();*/
    }

    public static Texture2D LoadPNG(string filePath)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = null;
            fileData = File.ReadAllBytes(filePath);
            /*Thread t = new Thread(() =>
            {
                fileData = File.ReadAllBytes(filePath);
            });
            t.IsBackground = true;
            t.Priority = ThreadPriority.Highest;
            t.Start();
            t.Join();*/
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }

    public static Texture2D CompressTexture(Texture2D mainTex, int targetWidth = 2048)
    {
        var newTex = mainTex;
        if (mainTex.width > targetWidth)
        {
            int targetHeight = mainTex.height / (mainTex.width / targetWidth);
            newTex = CustomTextureScale(mainTex, targetWidth, targetHeight);
            //            TextureScale.Bilinear(newTex, targetWidth, targetHeight);
        }
        return newTex;
    }

    /// <summary>
    ///     Returns a scaled copy of given texture.
    /// </summary>
    /// <param name="tex">Source texure to scale</param>
    /// <param name="width">Destination texture width</param>
    /// <param name="height">Destination texture height</param>
    /// <param name="mode">Filtering mode</param>
    public static Texture2D CustomTextureScale(Texture2D src, int width, int height, FilterMode mode = FilterMode.Trilinear)
    {
        Rect texR = new Rect(0, 0, width, height);
        _gpu_scale(src, width, height, mode);

        //Get rendered data back to a new texture
        Texture2D result = new Texture2D(width, height, TextureFormat.ARGB32, true);
        result.Resize(width, height);
        result.ReadPixels(texR, 0, 0, true);
        return result;
    }

    // Internal unility that renders the source texture into the RTT - the scaling method itself.
    static void _gpu_scale(Texture2D src, int width, int height, FilterMode fmode)
    {
        //We need the source texture in VRAM because we render with it
        src.filterMode = fmode;
        src.Apply(true);

        //Using RTT for best quality and performance. Thanks, Unity 5
        RenderTexture rtt = new RenderTexture(width, height, 32);

        //Set the RTT in order to render to it
        Graphics.SetRenderTarget(rtt);

        //Setup 2D matrix in range 0..1, so nobody needs to care about sized
        GL.LoadPixelMatrix(0, 1, 1, 0);

        //Then clear & draw the texture to fill the entire RTT.
        GL.Clear(true, true, new Color(0, 0, 0, 0));
        Graphics.DrawTexture(new Rect(0, 0, 1, 1), src);
    }
    #endregion
}
