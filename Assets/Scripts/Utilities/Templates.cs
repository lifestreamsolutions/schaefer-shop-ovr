﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ProjectDetailsTemplate
{
    public string projectID;
    public float currentProjectVersion;

    //    public string title;
    public Texture2D iconImage;

    public int id;
    public string name;
    [TextArea]
    public string description;

    public List<ProjectDetailsTemplate> variants;
    public int parentProjectIndex;
}

[Serializable]
public class HotSpotDetailsTemplate
{
    public string id;
    public string label;
    public int value;
    public Vector3 coOrdinates;
    public bool isStartPoint;
    public MediaType mediaType;
}

#region ProjectData

[Serializable]
public class ProjectDataTemplate
{
    public ProjectData data;
}

[Serializable]
public class ProjectData
{
    public string barcode;
    //    public int id ;
    public string name;
    public string customer_name;
    public string description;
    public string icon;
    public string hot_spot_icon;
    //    public string customer_logo;
    public SubProjectDataTemplate version;
}

[Serializable]
public class SubProjectDataTemplate
{
    public List<SubProjectData> data;
}

[Serializable]
public class SubProjectData
{
    public int id;
    public float currentProjectVersion;
    public string name;
    public string description;
    public string customer_name;
    public object email;
    public Panoramas media;
    public Map map;
    public string unique_id;
    public string icon;
    public string hot_spot_icon;
    public string version;
    public float mediaFullSize;
    //    public string customer_logo;
}

[Serializable]
public class Panoramas
{
    public List<PanoramaData> data;
}

[Serializable]
public class PanoramaData
{
    public int id;
    public string url;
    public bool is_start_point;
    public HotSpots transmission;
    public float x;//start rotation along y axis
    public float y;//start rotation along x axis
}

[Serializable]
public class HotSpots
{
    public List<HotSpotData> data;
}

[Serializable]
public class HotSpotData
{
    public int id;
    public string type;
    public int value;
    public string title;
    public float x;
    public float y;
    public int media_id;//for min map only
}

[Serializable]
public class Map
{
    public MapData data;
}

[Serializable]
public class MapData
{
    public string url;
    public HotSpots transmission;
}

[Serializable]
public class ProjectUpdateDetails
{
    public string projectID;
    public List<SubProjectUpdateDetails> subProjects;
}

[Serializable]
public class SubProjectUpdateDetails
{
    public int id;
    public string unique_id;
    public int currentProjectVersion;
}

[Serializable]
public class ProjectUpdateDetailsTemplate
{
    public bool success;
    public List<SubProjectUpdateDetails> data;
}
#endregion