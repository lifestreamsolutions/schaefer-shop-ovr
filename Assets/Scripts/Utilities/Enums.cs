﻿public enum AppMode
{
    Test = 0,
    Production
}

public enum ProjectType
{
    Demo,
    Normal
}

public enum MediaType
{
    Panorama,
    Video
}