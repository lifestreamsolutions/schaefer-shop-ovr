﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float speed = 1;
    // Use this for initialization
    void Start()
    {
    }

#if UNITY_EDITOR
    // Update is called once per frame
    void Update()
    {
        transform.eulerAngles += Input.GetAxis("Mouse X") * Vector3.up * speed;
        transform.eulerAngles += Input.GetAxis("Mouse Y") * Vector3.left * speed;
    }
#endif
}
