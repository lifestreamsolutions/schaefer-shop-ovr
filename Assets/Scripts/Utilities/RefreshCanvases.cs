﻿/*using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
 * HACK: This class is used to flush the VRAM in cardboard mode.
 * Unity is broken since v5.3.4p2 and a camera with a RenderTexture makes the VRAM leaks:
 * The memory increased continously until crashes (reproductible on iOS and Android)
 * https://issuetracker.unity3d.com/issues/googlevr-canvas-are-allocating-memory-and-do-not-release-it-when-using-vr-cameras
 *
 * Status:
 * the object that's leaking memory is the Canvas component, this fix will remove the canvas component and re-add it which releases the leaked memory.  
 * However since CanvasScaler & CanvasGraphicRay components are dependent on the Canvas we have to remove those as well and re-add them in.'
 * After adding back the components, we need to copy values back over
 *
 * IMPORTANT:  OTHER OBJECTS REFERENCING THE CANVAS AND DEPENDENT COMPONENTS WILL LOSE THEIR REFERENCE!
 #1#

public class RefreshCanvases : MonoBehaviour
{

    [SerializeField]
    private float RefreshTime = 15;

    private class CanvasCopyFields
    {
        private RenderMode renderMode;
        private string sortingLayerName;
        private int sortingLayerID;
        private int sortingOrder;
        private Camera worldCamera;

        public void CopyFrom(Canvas canvasToCopy)
        {
            renderMode = canvasToCopy.renderMode;
            sortingLayerName = canvasToCopy.sortingLayerName;
            sortingLayerID = canvasToCopy.sortingLayerID;
            sortingOrder = canvasToCopy.sortingOrder;
            worldCamera = canvasToCopy.worldCamera;
        }

        public void CopyTo(Canvas canvas)
        {
            canvas.renderMode = renderMode;
            canvas.sortingLayerName = sortingLayerName;
            canvas.sortingLayerID = sortingLayerID;
            canvas.sortingOrder = sortingOrder;
            canvas.worldCamera = worldCamera;
        }
    };

    private class ScalerCopyFields
    {
        private CanvasScaler.ScaleMode uiScaleMode;
        private float referencePixelsPerUnit;
        private float dynamicPixelsPerUnit;

        public void CopyFrom(CanvasScaler scalerToCopy)
        {
            uiScaleMode = scalerToCopy.uiScaleMode;
            referencePixelsPerUnit = scalerToCopy.referencePixelsPerUnit;
            dynamicPixelsPerUnit = scalerToCopy.dynamicPixelsPerUnit;
        }

        public void CopyTo(CanvasScaler scaler)
        {
            scaler.uiScaleMode = uiScaleMode;
            scaler.referencePixelsPerUnit = referencePixelsPerUnit;
            scaler.dynamicPixelsPerUnit = dynamicPixelsPerUnit;
        }
    };

    private class GraphicRaycasterCopyFields
    {
        private bool ignoreReversedGraphics;
        private GraphicRaycaster.BlockingObjects blockingObjects;

        public void CopyFrom(GraphicRaycaster raycasterToCopy)
        {
            ignoreReversedGraphics = raycasterToCopy.ignoreReversedGraphics;
            blockingObjects = raycasterToCopy.blockingObjects;
        }

        public void CopyTo(GraphicRaycaster raycaster)
        {
            raycaster.ignoreReversedGraphics = ignoreReversedGraphics;
            raycaster.blockingObjects = blockingObjects;
        }
    };

    private CanvasCopyFields copyCanvas = new CanvasCopyFields();
    private ScalerCopyFields copyCanvasScaler = new ScalerCopyFields();
    private GraphicRaycasterCopyFields copyGraphicRaycaster = new GraphicRaycasterCopyFields();

    private void Start()
    {
        StartCoroutine(RefreshCanvasesCoroutine());
    }

    void OnDestroy()
    {
        StopCoroutine(RefreshCanvasesCoroutine());
    }

    private IEnumerator RefreshCanvasesCoroutine()
    {
        while (true)
        {
            if (GvrViewer.Instance.VRModeEnabled == true)
            {
                Canvas[] allCanvas = GameObject.FindObjectsOfType<Canvas>();

                foreach (Canvas curCanvas in allCanvas)
                {

                    // Issue only occurs for worldspace canvases
                    if (curCanvas.renderMode != RenderMode.WorldSpace)
                    {
                        continue;
                    }

                    // Step 1: Copy and destroy the canvas & dependent components
                    copyCanvas.CopyFrom(curCanvas);
                    GameObject canvasObj = curCanvas.gameObject;

                    bool hasScaler = false;
                    CanvasScaler lastScaler = canvasObj.GetComponent<CanvasScaler>();

                    if (lastScaler != null)
                    {
                        hasScaler = true;
                        copyCanvasScaler.CopyFrom(lastScaler);
                        DestroyImmediate(lastScaler);
                    }

                    bool hasRaycaster = false;
                    GraphicRaycaster lastRayCaster = canvasObj.GetComponent<GraphicRaycaster>();

                    if (lastRayCaster != null)
                    {
                        hasRaycaster = true;
                        copyGraphicRaycaster.CopyFrom(lastRayCaster);
                        DestroyImmediate(lastRayCaster);
                    }

                    DestroyImmediate(curCanvas);

                    // Step 2: Recreate the canvas & dependent components, and copy data back
                    Canvas newCanvas = canvasObj.AddComponent<Canvas>();
                    copyCanvas.CopyTo(newCanvas);

                    if (hasScaler)
                    {
                        CanvasScaler newScaler = canvasObj.AddComponent<CanvasScaler>();
                        copyCanvasScaler.CopyTo(newScaler);
                    }

                    if (hasRaycaster)
                    {
                        GraphicRaycaster newRaycaster = canvasObj.AddComponent<GraphicRaycaster>();
                        copyGraphicRaycaster.CopyTo(newRaycaster);
                    }
                }
            }

            yield return new WaitForSeconds(RefreshTime);
        }
    }
}*/