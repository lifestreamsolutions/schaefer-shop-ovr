﻿using System.Collections.Generic;

public class AlmostAStack<T>
{
    public List<T> items = new List<T>();

    public void Push(T item)
    {
        items.Add(item);
    }
    public T Pop()
    {
        if (items.Count > 0)
        {
            T temp = items[items.Count - 1];
            items.RemoveAt(items.Count - 1);
            return temp;
        }
        else
            return default(T);
    }
    public void Remove(int itemAtPosition)
    {
        items.RemoveAt(itemAtPosition);
    }

    public bool Contains(T t)
    {
        return items.Contains(t);
    }

    public T GetLastElement()
    {
        return items[items.Count - 1];
    }
}
