﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Bson;

public class ObjectPooler : MonoBehaviour
{
    #region Public_Variables
    public GameObject targetObject;
    public int pooledAmount;// at start
    public bool canPoolGrow;
    public int maxGrowAmount = -1;//no of grow, -1 = unlimited
    #endregion

    #region Private_Variables
    List<GameObject> _nonUsedPooledObjectList;
    List<GameObject> _usedPooledObjectList;
    GameObject _tempGameObject;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake()
    {
        InitPool();
    }

    void OnEnable()
    {
    }

    void OnDisable()
    {
    }

    // Use this for initialization
    void Start()
    {
    }

    #endregion

    #region Private_Methods

    void InitPool()
    {
        _nonUsedPooledObjectList = new List<GameObject>();
        _usedPooledObjectList = new List<GameObject>();
        for (int i = 0; i < pooledAmount; i++)
        {
            _nonUsedPooledObjectList.Add(InitObject());
        }
    }

    GameObject InitObject()
    {
        GameObject temp = (GameObject)Instantiate(targetObject);
        temp.transform.SetParent(transform);
        temp.SetActive(false);
        return temp;
    }
    #endregion

    #region Public_Methods

    public GameObject GetPooledObject()
    {
        /* for (int i = 0; i < _nonUsedPooledObjectList.Count; i++)
         {
             if (!_nonUsedPooledObjectList[i].activeInHierarchy)
             {
                 return _nonUsedPooledObjectList[i];
             }
         }*/
        _tempGameObject = null;
        if (_nonUsedPooledObjectList.Count > 0)
        {
            _tempGameObject = _nonUsedPooledObjectList[0];
            _nonUsedPooledObjectList.Remove(_tempGameObject);
            _usedPooledObjectList.Add(_tempGameObject);
        }

        if (_tempGameObject == null && canPoolGrow &&
            (maxGrowAmount == -1 || (_nonUsedPooledObjectList.Count + _usedPooledObjectList.Count) < (maxGrowAmount + pooledAmount)))
        {
            _tempGameObject = InitObject();//(GameObject)Instantiate(targetObject);
            _usedPooledObjectList.Add(_tempGameObject);
        }
        return _tempGameObject;
    }

    public void DestroyPooledObject(GameObject go)
    {
        go.SetActive(false);
        go.transform.SetParent(transform);
        _usedPooledObjectList.Remove(go);
        _nonUsedPooledObjectList.Add(go);
    }

    public void DisposeAllPooledObjects()
    {
        for (int i = 0; i < _usedPooledObjectList.Count; i++)
        {
            DestroyPooledObject(_usedPooledObjectList[0]);
        }
    }
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    #endregion
}
