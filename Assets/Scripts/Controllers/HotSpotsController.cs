﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class HotSpotsController : MonoBehaviour
{
    public static HotSpotsController instance;

    #region Public_Variables
    public ObjectPooler hotSpotObjectPooler;
    public Transform hotSpotPanel;

    public List<HotSpotDetailsTemplate> hotSpotDetails = new List<HotSpotDetailsTemplate>();
    public Sprite defaultHotspotIconSprite;

    public Sprite HotspotIconSprite
    {
        get { return _hotspotIconSprite; }
    }
    #endregion

    #region Private_Variables
    UIFadeInOut _uiFadeInOut;//hotspot panel
    Sprite _hotspotIconSprite;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        EventManager.ProjectOpened += OnProjectOpened;
    }
    // Use this for initialization
    void Start()
    {
        _uiFadeInOut = hotSpotPanel.GetComponent<UIFadeInOut>();
        //        ShowAllHotSpots();
    }

    void OnDisable()
    {
        EventManager.ProjectOpened -= OnProjectOpened;
    }
    #endregion

    #region Private_Methods

    void InstantiateHotSpot(HotSpotDetailsTemplate hotSpotDetails)
    {
        GameObject hotSpot = hotSpotObjectPooler.GetPooledObject();
        hotSpot.transform.SetParent(hotSpotPanel);
        hotSpot.transform.ResetTransfrom();
        hotSpot.SetActive(true);
        if (_hotspotIconSprite)
            hotSpot.transform.FindChild("Button").GetComponent<Image>().sprite = _hotspotIconSprite;
        hotSpot.GetComponent<HotSpot>().SetDetails(hotSpotDetails);
        hotSpot.GetComponent<HotSpot>().SetOnClickAction(() =>
        {
            AppManager.instance.LoadPanoramaDataBasedOnPanoId(hotSpotDetails.value);
        });
    }

    void ShowAllHotSpots()
    {
        if (!_uiFadeInOut)
            _uiFadeInOut = hotSpotPanel.GetComponent<UIFadeInOut>();

        _uiFadeInOut.StartFadeOut(0.35f, () =>
         {
             hotSpotPanel.transform.DisableAllChilds();
             hotSpotObjectPooler.DisposeAllPooledObjects();

             foreach (var hotSpot in hotSpotDetails)
             {
                 InstantiateHotSpot(hotSpot);
             }
             _uiFadeInOut.StartFadeIn(0.35f, null);
             Debug.Log("Panorama : HotSpots shown");
         });
    }

    void LoadHotSpotIcon()
    {
        try
        {
            _hotspotIconSprite = null;
            string path = Path.Combine(GetHotSpotIconFolderPath(AppManager.instance.currentSelectedProjectType),
                Constants.HotSpot_Icon_Image_Name + Constants.PNG_Ext);
            Texture tex = Utilities.LoadPNG(path);
            if (tex != null)
            {
                _hotspotIconSprite = Sprite.Create((Texture2D)tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
            }
            else
            {
                _hotspotIconSprite = defaultHotspotIconSprite;
            }
            Debug.Log("HotSpot Icon Loaded");
        }
        catch
        {
            _hotspotIconSprite = defaultHotspotIconSprite;
        }
    }

    string GetHotSpotIconFolderPath(ProjectType projectType)
    {
        string basePath = "";
        if (projectType.Equals(ProjectType.Demo))
        {
            basePath = Path.Combine(Application.persistentDataPath, Constants.DemoProjects_Folder_Name);
        }
        else
        {
            basePath = Application.persistentDataPath;
        }
        string folderPath = Path.Combine(Path.Combine(basePath, Constants.ProjectData_Folder_Name),
            AppManager.instance.CurrentSubProjectID_Selected);
        return folderPath;
    }
    #endregion

    #region Public_Methods
    public void ShowHotSpotsBasedOnCurrentPano(PanoramaData panoramaData)
    {
        hotSpotDetails.Clear();
        foreach (var hs in panoramaData.transmission.data)
        {
            HotSpotDetailsTemplate hsdt = new HotSpotDetailsTemplate();
            hsdt.label = hs.title;
            hsdt.value = hs.value;
            //here coOrdinates used for rotation
            hsdt.coOrdinates = new Vector2(hs.y, hs.x); //krpano, x=ath, y=atv
            hotSpotDetails.Add(hsdt);
        }
        ShowAllHotSpots();
    }
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks

    void OnProjectOpened()
    {
        LoadHotSpotIcon();
    }
    #endregion
}
