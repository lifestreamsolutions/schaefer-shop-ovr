﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    public static MapController instance;

    #region Public_Variables
    public Transform mapCanvas;
    public GameObject mapPanel;
    public RawImage mapRawImage;
    public ObjectPooler hotSpotObjectPooler;
    public Transform hotSpotPanel;
    public Transform mapButtonPivot;
    public Transform refCamera;//ref. used to rotate map button
    public List<HotSpotDetailsTemplate> hotSpotDetailsList = new List<HotSpotDetailsTemplate>();
    #endregion

    #region Private_Variables
    private UIFadeInOut _uiFadeInOut;//map panel
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        EventManager.ProjectOpened += OnProjectOpened;
        EventManager.ProjectClosed += OnProjectClosed;
    }

    // Use this for initialization
    void Start()
    {
        mapButtonPivot.gameObject.SetActive(false);
    }

#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            EnableMap();
        }
    }
#endif

    void LateUpdate()
    {
        if (!mapPanel.activeInHierarchy)
        {
            RotateMapCanvas();
        }
    }

    void OnDisable()
    {
        EventManager.ProjectOpened -= OnProjectOpened;
        EventManager.ProjectClosed -= OnProjectClosed;
    }
    #endregion

    #region Private_Methods
    string GetMapFolderPath(ProjectType projectType)
    {
        string basePath = "";
        if (projectType.Equals(ProjectType.Demo))
        {
            basePath = Path.Combine(Application.persistentDataPath, Constants.DemoProjects_Folder_Name);
        }
        else
        {
            basePath = Application.persistentDataPath;
        }
        string folderPath = Path.Combine(Path.Combine(basePath, Constants.ProjectData_Folder_Name),
            AppManager.instance.CurrentSubProjectID_Selected);
        return folderPath;
    }

    void ShowMap(MapData mapData)
    {
        if (mapData.transmission.data.Count > 0)
        {
            ToggleMapPanel(true);
            LoadMapFromLocal();
            ShowAllHotSpots(mapData.transmission);
        }
        else
        {
            Debug.Log("No Map-Data found");
        }
    }

    void ToggleMapPanel(bool enable)
    {
        mapPanel.SetActive(enable);
    }

    void LoadMapFromLocal()
    {
        string path = Path.Combine(GetMapFolderPath(AppManager.instance.currentSelectedProjectType),
            Constants.Map_Image_Name + Constants.PNG_Ext);
        Texture tex = Utilities.LoadPNG(path);
        if (AppManager.instance.NeedToCompressImage())
        {
            tex = Utilities.CompressTexture(tex as Texture2D);
        }
        SetMapToImage(tex);
        Debug.Log("Map Loaded");
    }

    void SetMapToImage(Texture map)
    {
        mapRawImage.texture = map;
        mapRawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(mapRawImage.texture.width, mapRawImage.texture.height);
    }

    void InstantiateHotSpot(HotSpotDetailsTemplate hotSpotDetails)
    {
        GameObject hotSpot = hotSpotObjectPooler.GetPooledObject();
        hotSpot.transform.SetParent(hotSpotPanel);
        hotSpot.transform.ResetTransfrom();
        hotSpot.SetActive(true);
        if (HotSpotsController.instance.HotspotIconSprite)
            hotSpot.transform.FindChild("Button").GetComponent<Image>().sprite = HotSpotsController.instance.HotspotIconSprite;
        hotSpot.GetComponent<MapHotSpot>().SetDetails(hotSpotDetails);
        hotSpot.GetComponent<MapHotSpot>().SetOnClickAction(() =>
        {
            AppManager.instance.LoadPanoramaDataBasedOnPanoId(hotSpotDetails.value);
            mapPanel.SetActive(false);
        });
    }

    void ShowAllHotSpots(HotSpots hotSpots)
    {
        if (!_uiFadeInOut)
            _uiFadeInOut = mapPanel.GetComponent<UIFadeInOut>();

        List<HotSpotData> hotSpotData = new List<HotSpotData>(hotSpots.data);
        hotSpotDetailsList.Clear();
        foreach (var hs in hotSpotData)
        {
            HotSpotDetailsTemplate hsdt = new HotSpotDetailsTemplate();
            hsdt.id = hs.id.ToString();
            hsdt.value = hs.media_id;
            //multiplier to coOrdinates for getting appropriate pos
            Vector2 multiplier = new Vector2(mapRawImage.texture.width, mapRawImage.texture.height);
            //here coOrdinates used for position
            hsdt.coOrdinates = new Vector2(hs.x * multiplier.x, -hs.y * multiplier.y);
            hotSpotDetailsList.Add(hsdt);
        }

        hotSpotPanel.transform.DisableAllChilds();
        hotSpotObjectPooler.DisposeAllPooledObjects();

        foreach (var hotSpot in hotSpotDetailsList)
        {
            InstantiateHotSpot(hotSpot);
        }

        Debug.Log("Map : HotSpots shown");
        _uiFadeInOut.StartFadeIn(0.35f, null);
    }

    void RotateMapCanvas()
    {
        mapCanvas.rotation = Quaternion.Lerp(mapCanvas.rotation,
            Quaternion.Euler(mapCanvas.eulerAngles.x, refCamera.eulerAngles.y, mapCanvas.eulerAngles.z),
            2 * Time.deltaTime);
    }
    #endregion

    #region Public_Methods
    public void EnableMap()
    {
        ShowMap(DataManager.instance.currentSelectedSubProjectData.map.data);
    }

    public void HideMap()
    {
        _uiFadeInOut.StartFadeOut(0.35f, () =>
        {
            Destroy(mapRawImage.texture);
            ToggleMapPanel(false);
        });
    }
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    void OnProjectOpened()
    {
        MapData mapData = DataManager.instance.currentSelectedSubProjectData.map.data;
        mapButtonPivot.gameObject.SetActive(mapData.transmission.data.Count > 0);
    }

    void OnProjectClosed()
    {
        ToggleMapPanel(false);
    }
    #endregion
}
