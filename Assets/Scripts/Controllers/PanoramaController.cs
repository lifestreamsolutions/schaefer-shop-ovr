﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

public class PanoramaController : MonoBehaviour
{
    public static PanoramaController instance;
    #region Public_Variables
    public GameObject panoramaSphere;
    public Material panoramaMaterial;
    public Texture initialPanoramaTexture;
    public Texture blackTexture;
    public AnimationCurve panoramaAnimationCurve;
    public float panoramaAnimationDuration = 1;
    #endregion

    #region Private_Variables
    bool _isPanoramaAnimating = false;
    //    private int _currentPanoramaIndex = 0;
    //cached panos of current open project
    Dictionary<string, Texture> _cachedPanoramas = new Dictionary<string, Texture>();
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        EventManager.ProjectClosed += OnProjectClosed;
    }

    // Use this for initialization
    void Start()
    {
        SetInitialPanorama();
        TogglePanoramaSphere(false);
    }

    void OnDisable()
    {
        EventManager.ProjectClosed -= OnProjectClosed;
    }

    #endregion

    #region Private_Methods
    string GetPanoramaFolderPath(ProjectType projectType)
    {
        string basePath = "";
        if (projectType.Equals(ProjectType.Demo))
        {
            basePath = Path.Combine(Application.persistentDataPath, Constants.DemoProjects_Folder_Name);
        }
        else
        {
            basePath = Application.persistentDataPath;
        }
        string folderPath = Path.Combine(Path.Combine(basePath, Constants.ProjectData_Folder_Name),
            Path.Combine(AppManager.instance.CurrentSubProjectID_Selected, Constants.Panorama_Folder_Name));
        return folderPath;
    }

    void SetInitialPanorama()
    {
        panoramaMaterial.SetTexture("_TexMat1", initialPanoramaTexture);
        panoramaMaterial.SetTexture("_TexMat2", initialPanoramaTexture);
    }

    void SetBlackTexture()
    {
        panoramaMaterial.SetTexture("_TexMat1", blackTexture);
        panoramaMaterial.SetTexture("_TexMat2", blackTexture);
    }

    void CacheRelatedPanos(HotSpots transmissions)
    {
        foreach (var transmission in transmissions.data)
        {
            if (AppManager.instance.memoryWarningDetected)
            {
                AppManager.instance.memoryWarningDetected = false;
                GC.Collect();
                break;
            }
            if (!_cachedPanoramas.ContainsKey(transmission.value.ToString()))
            {
                string path = Path.Combine(GetPanoramaFolderPath(AppManager.instance.currentSelectedProjectType),
                    transmission.value + Constants.PNG_Ext);
                /*Thread t = new Thread(() =>
                {*/
                Texture tex = Utilities.LoadPNG(path);
                lock (_cachedPanoramas)
                {
                    _cachedPanoramas.Add(transmission.value.ToString(), tex);
                }
                /*});
                t.IsBackground = true;
                t.Start();*/
            }
        }
    }
    #endregion

    #region Public_Methods
    public void TogglePanoramaSphere(bool enable)
    {
        panoramaSphere.SetActive(enable);
    }

    public void ChangePanoramaTexture(Texture nextTexture)
    {
        panoramaMaterial.SetTexture("_TexMat2", nextTexture);
        StartCoroutine(AnimatePanoramaTexture());
    }

    public void LoadPanoramaFromLocal(string panoramaID, HotSpots transmissions)
    {
        if (AppManager.instance.memoryWarningDetected)
        {
            ClearCachedPanoramas();
            GC.Collect();
            AppManager.instance.memoryWarningDetected = false;
        }
        if (_cachedPanoramas.ContainsKey(panoramaID))
        {
            ChangePanoramaTexture(_cachedPanoramas[panoramaID]);
        }
        else
        {
            string path = Path.Combine(GetPanoramaFolderPath(AppManager.instance.currentSelectedProjectType),
                panoramaID + Constants.PNG_Ext);
            Texture tex = Utilities.LoadPNG(path);
            _cachedPanoramas.Add(panoramaID, tex);
            ChangePanoramaTexture(tex);
            Debug.Log("Panorama Cached : " + panoramaID);
        }
        CacheRelatedPanos(transmissions);
    }

    /// <summary>
    /// Clear cached panoramas, if new project is being loaded
    /// </summary>
    public void ClearCachedPanoramas()
    {
        for (int i = 0; i < _cachedPanoramas.Count; i++)
        {
            Destroy(_cachedPanoramas.ElementAt(i).Value);
        }
        _cachedPanoramas.Clear();
        Debug.Log("Clear Cached Panoramas");
        Resources.UnloadUnusedAssets();
        GC.Collect();
    }
    #endregion

    #region Coroutines
    IEnumerator AnimatePanoramaTexture()
    {
        _isPanoramaAnimating = true;
        float progress = 0;
        while (progress < 1)
        {
            progress += (Time.unscaledDeltaTime / panoramaAnimationDuration);
            panoramaMaterial.SetFloat("_Blend", panoramaAnimationCurve.Evaluate(progress));
            yield return null;
        }
        panoramaMaterial.SetTexture("_TexMat1", panoramaMaterial.GetTexture("_TexMat2"));
        panoramaMaterial.SetFloat("_Blend", 0);
        _isPanoramaAnimating = false;
    }
    #endregion

    #region Custom_CallBacks
    void OnProjectOpened()
    {
        TogglePanoramaSphere(true);
    }

    void OnProjectClosed()
    {
        TogglePanoramaSphere(false);
        SetBlackTexture();
        ClearCachedPanoramas();
    }
    #endregion
}
