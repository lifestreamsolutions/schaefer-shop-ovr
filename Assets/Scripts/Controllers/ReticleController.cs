﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReticleController : MonoBehaviour
{
    public static ReticleController Instance;

    #region Public_Variables
    public Image reticleOuter;
    public Image reticleInner;
    public bool autoHide = true;
    [Range(0f, 1f)]
    public float innerAlphaWhenIdle = 0.5f;
    #endregion

    #region Private_Variables
    private float _timeToAnimate;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake()
    {
        reticleOuter.fillAmount = 0;
        reticleInner.GetComponent<CanvasRenderer>().SetAlpha(innerAlphaWhenIdle);
        reticleOuter.transform.localScale = Vector3.one;
        Instance = this;
        if (autoHide)
        {
            reticleOuter.gameObject.SetActive(false);
        }
    }

    #endregion

    #region Private_Methods
    #endregion

    #region Public_Methods

    public void StartReticleAnimation(float time)
    {
        reticleOuter.fillAmount = 0;
        reticleInner.GetComponent<CanvasRenderer>().SetAlpha(0.5f);
        _timeToAnimate = time;
        if (autoHide)
            reticleOuter.gameObject.SetActive(true);
        if (gameObject.activeInHierarchy)
            StartCoroutine("ReticleAnimation");
    }

    public void ResetReticleAnimation()
    {
        reticleOuter.fillAmount = 0;
        reticleOuter.transform.localScale = Vector3.one;
        reticleInner.GetComponent<CanvasRenderer>().SetAlpha(innerAlphaWhenIdle);
        if (gameObject.activeInHierarchy)
            StopCoroutine("ReticleAnimation");
        if (autoHide)
            reticleOuter.gameObject.SetActive(false);
    }
    #endregion

    #region Coroutines

    private IEnumerator ReticleAnimation()
    {
        float progress = 0;
        float scaleAnimationTime = 0.5f;
        while (progress < 1)
        {
            progress += (Time.smoothDeltaTime / scaleAnimationTime);
            reticleOuter.transform.localScale = Vector3.one * progress;
            yield return null;
        }
        _timeToAnimate -= scaleAnimationTime;
        progress = 0;
        while (progress < 1)
        {
            progress += (Time.smoothDeltaTime / _timeToAnimate);
            reticleOuter.fillAmount = progress;
            yield return null;
        }
    }

    #endregion

    #region Custom_CallBacks
    #endregion
}
