﻿using System;
using UnityEngine;
using System.Collections;
using BarcodeScanner;
using BarcodeScanner.Scanner;
using UnityEngine.UI;
using Wizcorp.Utils.Logger;

public class BarcodeScannerController : MonoBehaviour
{
    public static BarcodeScannerController instance;

    #region Public_Variables
    //    public Text TextHeader;
    public RawImage targetRawImage;
    //    public AudioSource Audio;
    #endregion

    #region Private_Variables
    private IScanner BarcodeScanner;
    #endregion

    #region Events
    public delegate void BarcodeScanned(string barcodeScannerValue);
    public static event BarcodeScanned OnBarcodeScanned;

    private static void InvokeOnBarcodeScanned(string barcodeScannerValue)
    {
        var handler = OnBarcodeScanned;
        if (handler != null) handler(barcodeScannerValue);
    }
    #endregion

    #region Unity_CallBacks

    void Awake()
    {
        instance = this;
    }

    void OnEnable() { }

    // Use this for initialization
    IEnumerator Start()
    {
        // When the app start, ask for the authorization to use the webcam
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);

        if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            throw new Exception("This Webcam library can't work without the webcam authorization");
        }
    }

    /*// Update is called once per frame
    void Update()
    {
        if (BarcodeScanner == null)
        {
            return;
        }
        BarcodeScanner.Update();
    }*/
    void OnDisable() { }
    #endregion

    #region Private_Methods

    void Init()
    {
        // Create a basic scanner
        BarcodeScanner = new Scanner();
        BarcodeScanner.Camera.Play();

        // Display the camera texture through a RawImage
        BarcodeScanner.OnReady += (sender, arg) =>
        {
            // Set Orientation & Texture
            targetRawImage.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
            targetRawImage.transform.localScale = BarcodeScanner.Camera.GetScale();
            targetRawImage.texture = BarcodeScanner.Camera.Texture;

            /*// Keep Image Aspect Ratio
            var rect = targetRawImage.GetComponent<RectTransform>();
            var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);*/

            if (BarcodeScanner == null)
            {
                Log.Warning("No valid camera - Click Start");
                return;
            }

            // Start Scanning
            BarcodeScanner.Scan((barCodeType, barCodeValue) =>
            {
                BarcodeScanner.Stop();
                Debug.Log("Found: " + barCodeType + " / " + barCodeValue);
                //            TextHeader.text = "Found: " + barCodeType + " / " + barCodeValue;

                /*// Feedback
                Audio.Play();*/

#if UNITY_ANDROID || UNITY_IOS
                Handheld.Vibrate();
#endif
                InvokeOnBarcodeScanned(barCodeValue);
            });
        };

        // Track status of the scanner
        BarcodeScanner.StatusChanged += (sender, arg) =>
        {
            Debug.Log("Status: " + BarcodeScanner.Status);
            //            TextHeader.text = "Status: " + BarcodeScanner.Status;
        };
        StartCoroutine("UpdateEnumerator");
    }
    #endregion

    #region Public_Methods

    public void SetTargetRawImage(RawImage rawImage)
    {
        targetRawImage = rawImage;
    }

    public void StartScanning()
    {
        Init();
    }

    public void StopScanning()
    {
        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Stop");
            return;
        }
        StopCoroutine("UpdateEnumerator");

        // Stop Scanning
        BarcodeScanner.Stop();
        StartCoroutine(StopCamera(null));
    }
    #endregion

    #region Coroutines

    IEnumerator UpdateEnumerator()
    {
        while (true)
        {
            BarcodeScanner.Update();
            yield return null;
        }
    }

    /// <summary>
    /// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
    /// Trying to stop the camera in OnDestroy provoke random crash on Android
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    public IEnumerator StopCamera(Action callback)
    {
        // Stop Scanning
        targetRawImage = null;
        BarcodeScanner.Destroy();
        BarcodeScanner = null;

        // Wait a bit
        yield return new WaitForSeconds(0.1f);
        if (callback != null)
            callback.Invoke();
    }
    #endregion

    #region Custom_CallBacks
    #endregion
}
