﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VirtualKeyboardController : MonoBehaviour
{
    public static VirtualKeyboardController instance;

    #region Public_Variables
    public Text keyboardText;
    public string keyboardTextString;
    #endregion

    #region Private_Variables
    #endregion

    #region Events

    public delegate void KeyBoardTextChanged(string text);
    public static event KeyBoardTextChanged OnKeyBoardTextChanged;

    private static void InvokeOnKeyBoardTextChanged(string text)
    {
        var handler = OnKeyBoardTextChanged;
        if (handler != null) handler(text);
    }
    #endregion

    #region Unity_CallBacks
    void Awake()
    {
        instance = this;
    }
    void OnEnable() { }
    // Use this for initialization
    void Start() { }
    void OnDisable() { }
    #endregion

    #region Private_Methods

    void UpdateTextComponent()
    {
        keyboardText.text = keyboardTextString;
    }
    #endregion

    #region Public_Methods
    public void SetDefaultString(string text)
    {
        keyboardTextString = text;
        UpdateTextComponent();
        InvokeOnKeyBoardTextChanged(keyboardTextString);
    }

    public void ClearKeyboardTextString()
    {
        keyboardTextString = "";
        UpdateTextComponent();
        InvokeOnKeyBoardTextChanged(keyboardTextString);
    }

    public void BackSpaceClicked()
    {
        if (keyboardTextString.Length > 0)
        {
            keyboardTextString = keyboardTextString.Substring(0, keyboardTextString.Length - 1);
            UpdateTextComponent();
            InvokeOnKeyBoardTextChanged(keyboardTextString);
        }
    }

    public void OnKeyClicked(Text keyText)
    {
        keyboardTextString += keyText.text;
        UpdateTextComponent();
        InvokeOnKeyBoardTextChanged(keyboardTextString);
    }
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    #endregion
}
