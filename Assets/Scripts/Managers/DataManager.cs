﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

public class DataManager : MonoBehaviour
{
    public static DataManager instance;

    #region Public_Variables
    public List<string> demoProjectIDs = new List<string>();
    [Space]
    public List<string> downloadedProjectIDs = new List<string>();
    [Space]
    public List<ProjectDetailsTemplate> demoProjects = new List<ProjectDetailsTemplate>();
    [Space]
    public List<ProjectDetailsTemplate> projects = new List<ProjectDetailsTemplate>();
    [Space]
    public List<ProjectUpdateDetails> updateAvailableForProjects = new List<ProjectUpdateDetails>();
    [Space]
    public ProjectDataTemplate currentSelectedProjectData;
    public SubProjectData currentSelectedSubProjectData;
    [Space]
    [Header("Download Progress Properties")]
    [Space]
    [Range(0f, 100f)]
    public float downloadProgress = 0;
    public float noOfDownloads = 0;
    public float noOfDownloaded = 0;
    #endregion

    #region Private_Variables
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        EventManager.MoveFromSAToPDComplete += EnlistDemoProjects;
        EventManager.ProjectOpened += OnProjectOpened;
    }

    // Use this for initialization
    void Start()
    {
        //                        DeleteAllProjects();
        EnlistDownloadedProjects();
        CheckUpdateAvailabilityForProjects();
        //        EnlistDemoProjects();
    }

    void OnDisable()
    {
        EventManager.MoveFromSAToPDComplete -= EnlistDemoProjects;
        EventManager.ProjectOpened -= OnProjectOpened;
    }
    #endregion

    #region Private_Methods

    string GetProjectDataDetailsFolderPath(ProjectType projectType)
    {
        string basePath = "";
        if (projectType.Equals(ProjectType.Demo))
        {
            basePath = Path.Combine(Application.persistentDataPath, Constants.DemoProjects_Folder_Name);
        }
        else
        {
            basePath = Application.persistentDataPath;
        }
        string folderPath = Path.Combine(basePath, Constants.ProjectDataDetails_Folder_Name);
        return folderPath;
    }

    string GetProjectDataFolderPath(ProjectType projectType)
    {
        string basePath = "";
        if (projectType.Equals(ProjectType.Demo))
        {
            basePath = Path.Combine(Application.persistentDataPath, Constants.DemoProjects_Folder_Name);
        }
        else
        {
            basePath = Application.persistentDataPath;
        }
        string folderPath = Path.Combine(basePath, Constants.ProjectData_Folder_Name);
        return folderPath;
    }

    /* void DeleteAllProjects()
     {
         DeleteAllProjectDetails();
         DeleteAllProjectData();
     }

     void DeleteAllProjectDetails()
     {
         string path = GetProjectDataDetailsFolderPath(ProjectType.Normal);
         if (Directory.Exists(path))
         {
             var info = new DirectoryInfo(path);
             info.Delete(true);

             downloadedProjectIDs.Clear();
             projects.Clear();
             updateAvailableForProjects.Clear();
         }

         Debug.Log("Project Details Deleted.");
     }

     void DeleteAllProjectData()
     {
         string path = GetProjectDataFolderPath(ProjectType.Normal);
         if (Directory.Exists(path))
         {
             var info = new DirectoryInfo(path);
             info.Delete(true);

             downloadedProjectIDs.Clear();
             projects.Clear();
             updateAvailableForProjects.Clear();
         }

         Debug.Log("Project Data Deleted.");
     }*/

    void LoadProjectDetails()
    {
        projects.Clear();
        foreach (var projectID in downloadedProjectIDs)
        {
            AppManager.instance.CurrentProjectID_Selected = projectID;
            LoadProjectDataFromLocal(ProjectType.Normal);
            if (currentSelectedProjectData != null)
            {
                projects.Add(CreateProjectDetails(ProjectType.Normal));
            }
        }

        AppManager.instance.CurrentProjectID_Selected = string.Empty;
        currentSelectedProjectData = null;
        Debug.Log("--- Project Details Loaded ---");
    }

    void EnlistDownloadedProjects()
    {
        try
        {
            string path = GetProjectDataDetailsFolderPath(ProjectType.Normal);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            downloadedProjectIDs.Clear();
            var info = new DirectoryInfo(path);
            var fileInfo = info.GetFiles();
            fileInfo = fileInfo.OrderByDescending(x => x.CreationTime).ToArray();
            foreach (var file in fileInfo)
            {
                downloadedProjectIDs.Add(file.Name);
            }
            Debug.Log("--- Downloaded Projects Enlisted ---");
            LoadProjectDetails();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    void LoadDemoProjectDetails()
    {
        demoProjects.Clear();
        foreach (var projectID in demoProjectIDs)
        {
            AppManager.instance.CurrentProjectID_Selected = projectID;
            LoadProjectDataFromLocal(ProjectType.Demo);
            if (currentSelectedProjectData != null)
            {
                demoProjects.Add(CreateProjectDetails(ProjectType.Demo));
            }
        }

        AppManager.instance.CurrentProjectID_Selected = string.Empty;
        currentSelectedProjectData = null;
        Debug.Log("--- Demo Project Details Loaded ---");
    }

    void EnlistDemoProjects()
    {
        try
        {
            string path = GetProjectDataDetailsFolderPath(ProjectType.Demo);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            demoProjectIDs.Clear();
            var info = new DirectoryInfo(path);
            var fileInfo = info.GetFiles();
            fileInfo = fileInfo.OrderByDescending(x => x.CreationTime).ToArray();
            foreach (var file in fileInfo)
            {
                if (string.IsNullOrEmpty(file.Extension))
                    demoProjectIDs.Add(file.Name);
            }
            Debug.Log("--- Demo Projects Enlisted ---");
            LoadDemoProjectDetails();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    ProjectDetailsTemplate CreateProjectDetails(ProjectType projectType)
    {
        try
        {
            ProjectDetailsTemplate projectDetails = new ProjectDetailsTemplate();
            projectDetails.projectID = currentSelectedProjectData.data.barcode;
            projectDetails.name = FixSchaferName(currentSelectedProjectData.data.name);
            projectDetails.description = FixSchaferName(currentSelectedProjectData.data.description);
            projectDetails.variants = new List<ProjectDetailsTemplate>();

            string dataPath = GetProjectDataFolderPath(projectType);

            string projectIconPath = Path.Combine(dataPath, projectDetails.projectID);
            projectIconPath = Path.Combine(projectIconPath, Constants.Project_Icon_Image_Name + Constants.PNG_Ext);
            projectDetails.iconImage = Utilities.LoadPNG(projectIconPath);

            foreach (var subProjectTemp in currentSelectedProjectData.data.version.data)
            {
                ProjectDetailsTemplate subProjectDetails = new ProjectDetailsTemplate();
                subProjectDetails.projectID = subProjectTemp.unique_id;
                //                subProjectDetails.name = FixSchaferName(subProjectTemp.name);
                subProjectDetails.name = FixSchaferName(subProjectTemp.version);
                subProjectDetails.description = FixSchaferName(subProjectTemp.description);
                subProjectDetails.currentProjectVersion = subProjectTemp.currentProjectVersion;

                string subProjectIconPath = Path.Combine(dataPath, Path.Combine(projectDetails.projectID, subProjectDetails.projectID));
                subProjectIconPath = Path.Combine(subProjectIconPath, Constants.Project_Icon_Image_Name + Constants.PNG_Ext);
                subProjectDetails.iconImage = Utilities.LoadPNG(subProjectIconPath);

                projectDetails.variants.Add(subProjectDetails);
            }
            return projectDetails;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return null;
        }
    }

    string FixSchaferName(string oldText)
    {
        string newText = oldText;
        string pattern = "Schaefer";
        if (oldText.Contains(pattern))
        {
            newText = newText.Replace(pattern, Constants.Schafer_Label);
        }

        pattern = "Schafer";
        if (oldText.Contains(pattern))
        {
            newText = newText.Replace(pattern, Constants.Schafer_Label);
        }
        return newText;
    }

    void LoadProjectDataFromLocal(ProjectType projectType)
    {
        try
        {
            string path = GetProjectDataDetailsFolderPath(projectType);
            string fileName = Path.Combine(path, AppManager.instance.CurrentProjectID_Selected);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (File.Exists(fileName))
            {
                string projData = File.ReadAllText(fileName);
                projData = projData.ToDecodedString();
                currentSelectedProjectData = JsonConvert.DeserializeObject<ProjectDataTemplate>(projData);
                Debug.Log("> Load Project Data : " + AppManager.instance.CurrentProjectID_Selected);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    void LoadSubProjectDataFromLocal(ProjectType projectType)
    {
        try
        {
            string[] pathStrings = AppManager.instance.CurrentSubProjectID_Selected.Split(Path.DirectorySeparatorChar);
            string parentPath = "";
            parentPath = pathStrings[0];
            if (pathStrings.Length > 2)
                for (int i = 1; i < pathStrings.Length - 1; i++)
                {
                    parentPath += Path.Combine(parentPath, pathStrings[i]);
                }
            string path = GetProjectDataDetailsFolderPath(projectType);
            string fileName = Path.Combine(path, pathStrings[0]);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (File.Exists(fileName))
            {
                string projData = File.ReadAllText(fileName);
                projData = projData.ToDecodedString();
                currentSelectedSubProjectData = JsonConvert.DeserializeObject<ProjectDataTemplate>(projData)
                    .data.version.data.Find(x => AppManager.instance.CurrentSubProjectID_Selected.Contains(x.unique_id));

                Debug.Log("> Load Sub Project Data : " + pathStrings[pathStrings.Length - 1]);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    void SaveProjectDataToLocal(string projectId, string projectData)
    {
        try
        {
            string path = GetProjectDataDetailsFolderPath(ProjectType.Normal);
            string fileName = Path.Combine(path, projectId/*AppManager.instance.CurrentProjectID_Download*/);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            File.WriteAllText(fileName, projectData.ToEncodedString());
            Debug.Log("> Save Project Data : " + projectId/*AppManager.instance.CurrentProjectID_Download*/);
            EnlistDownloadedProjects();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    void CheckUpdateAvailabilityForProjects()
    {
        updateAvailableForProjects.Clear();
        if (projects.Count > 0)
            APIManager.instance.CheckUpdateAvailabilityForProjects();
    }

    #endregion

    #region Public_Methods

    public void ProjectDataDownloaded(string projectId, string projectData)
    {
        SaveProjectDataToLocal(projectId, projectData);
        string prjDL = LocalizationManager.instance.GetLocalizedValue("Download Project");
        string prjDLSuccess = LocalizationManager.instance.GetLocalizedValue("Project Download Success Message");
        MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, prjDL, prjDLSuccess,
            null, null, Constants.Ok);
    }

    public void SubProjectDataUpdated(string projectId, string subProjectId, string projectData)
    {
        string path = GetProjectDataDetailsFolderPath(ProjectType.Normal);
        string fileName = Path.Combine(path, projectId);

        if (File.Exists(fileName))
        {
            string projData = File.ReadAllText(fileName);
            projData = projData.ToDecodedString();
            ProjectDataTemplate oldTemp = JsonConvert.DeserializeObject<ProjectDataTemplate>(projData);
            ProjectDataTemplate newTemp = JsonConvert.DeserializeObject<ProjectDataTemplate>(projectData);

            int a = oldTemp.data.version.data.FindIndex(x => x.unique_id.Equals(subProjectId));
            int b = newTemp.data.version.data.FindIndex(x => x.unique_id.Equals(subProjectId));

            oldTemp.data.version.data[a] = newTemp.data.version.data[b];
            SaveProjectDataToLocal(projectId, JsonConvert.SerializeObject(oldTemp));
            LoadProjectDetails();
            string prjUDT = LocalizationManager.instance.GetLocalizedValue("Update Project");
            string prjUDTSuccess = LocalizationManager.instance.GetLocalizedValue("Project Update Success Message");
            MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, prjUDT, prjUDTSuccess, null, null, Constants.Ok);
        }
    }

    public void ProjectDataUpdated(string projectId, string projectData)
    {
        string path = GetProjectDataDetailsFolderPath(ProjectType.Normal);
        string fileName = Path.Combine(path, projectId);

        if (File.Exists(fileName))
        {
            string projData = File.ReadAllText(fileName);
            projData = projData.ToDecodedString();
            ProjectDataTemplate updatedData = JsonConvert.DeserializeObject<ProjectDataTemplate>(projData);

            ProjectDataTemplate oldTemp = JsonConvert.DeserializeObject<ProjectDataTemplate>(projData);
            ProjectDataTemplate newTemp = JsonConvert.DeserializeObject<ProjectDataTemplate>(projectData);

            updatedData.data.version.data.Clear();

            //add new subprojects that are in newTemp only
            foreach (var sub in newTemp.data.version.data)
            {
                if (oldTemp.data.version.data.Find(x => x.unique_id.Equals(sub.unique_id)) == null)
                {
                    updatedData.data.version.data.Add(sub);
                }
            }

            //add subprojects from oldTemp that are in newTemp as well
            foreach (var sub in oldTemp.data.version.data)
            {
                if (newTemp.data.version.data.Find(x => x.unique_id.Equals(sub.unique_id)) != null)
                {
                    updatedData.data.version.data.Add(sub);
                }
            }

            //remove data of deleted subprojects
            foreach (var sub in oldTemp.data.version.data)
            {
                if (updatedData.data.version.data.Find(x => x.unique_id.Equals(sub.unique_id)) == null)
                {
                    string pathTemp = GetProjectDataFolderPath(ProjectType.Normal);
                    pathTemp = Path.Combine(pathTemp, Path.Combine(oldTemp.data.barcode, sub.unique_id));
                    if (Directory.Exists(pathTemp))
                    {
                        var info = new DirectoryInfo(pathTemp);
                        info.Delete(true);
                    }
                }
            }

            SaveProjectDataToLocal(projectId, JsonConvert.SerializeObject(updatedData));
            LoadProjectDetails();

            string prjUDT = LocalizationManager.instance.GetLocalizedValue("Update Project");
            string prjUDTSuccess = LocalizationManager.instance.GetLocalizedValue("Project Update Success Message");
            MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, prjUDT, prjUDTSuccess, null, null, Constants.Ok);
        }
    }

    public void AddProjectToAvailableUpdateList(string projectId, List<SubProjectUpdateDetails> subProjects)
    {
        ProjectUpdateDetails projectUpdateDetails = new ProjectUpdateDetails();
        projectUpdateDetails.projectID = projectId;
        projectUpdateDetails.subProjects = subProjects;
        updateAvailableForProjects.Add(projectUpdateDetails);
        Debug.Log("> Project Update Checked : " + projectId);
    }

    public void SavePanorama(string panoramaFolderPath, Texture2D texture, int id)
    {
        string fileName = id + Constants.PNG_Ext;
        string path = Path.Combine(panoramaFolderPath, fileName);

        if (!Directory.Exists(panoramaFolderPath))
        {
            Directory.CreateDirectory(panoramaFolderPath);
        }

        if (AppManager.instance.NeedToCompressImage())
        {
            texture = Utilities.CompressTexture(texture);
        }

        Utilities.SaveTextureAsPNG(texture, path);
        Destroy(texture);
        Resources.UnloadUnusedAssets();
    }

    public void SaveMap(string mapImagePath, Texture2D texture)
    {
        string fileName = Constants.Map_Image_Name + Constants.PNG_Ext;
        string path = Path.Combine(mapImagePath, fileName);

        if (!Directory.Exists(mapImagePath))
        {
            Directory.CreateDirectory(mapImagePath);
        }

        if (AppManager.instance.NeedToCompressImage())
        {
            texture = Utilities.CompressTexture(texture);
        }

        Utilities.SaveTextureAsPNG(texture, path);
        Destroy(texture);
        Resources.UnloadUnusedAssets();
    }

    public void SaveProjectIcon(string logoImagePath, Texture2D texture)
    {
        string fileName = Constants.Project_Icon_Image_Name + Constants.PNG_Ext;
        string path = Path.Combine(logoImagePath, fileName);

        if (!Directory.Exists(logoImagePath))
        {
            Directory.CreateDirectory(logoImagePath);
        }

        Utilities.SaveTextureAsPNG(texture, path);
        Destroy(texture);
        Resources.UnloadUnusedAssets();
    }

    public void SaveSubProjectHotSpotIcon(string logoImagePath, Texture2D texture)
    {
        string fileName = Constants.HotSpot_Icon_Image_Name + Constants.PNG_Ext;
        string path = Path.Combine(logoImagePath, fileName);

        if (!Directory.Exists(logoImagePath))
        {
            Directory.CreateDirectory(logoImagePath);
        }

        Utilities.SaveTextureAsPNG(texture, path);
        Destroy(texture);
        Resources.UnloadUnusedAssets();
    }

    public void CalculateNoOfDownloadsForProjectDownload(ProjectDataTemplate projectDataTemplate)
    {
        try
        {
            noOfDownloads = 0;
            downloadProgress = 0;

            if (!string.IsNullOrEmpty(projectDataTemplate.data.icon))
                noOfDownloads += 1; // project icon

            foreach (var version in projectDataTemplate.data.version.data)
            {
                if (!string.IsNullOrEmpty(version.icon))
                    noOfDownloads += 1;//sub project icon

                if (!string.IsNullOrEmpty(version.hot_spot_icon))
                    noOfDownloads += 1;//sub project hotspot icon

                noOfDownloads += version.media.data.Count;//sub project panos

                if (version.map != null && !string.IsNullOrEmpty(version.map.data.url))
                    noOfDownloads += 1;//sub project map
            }
            Debug.Log("Total no. of downloads : " + noOfDownloads);
        }
        catch
        {
        }
    }

    public void CalculateNoOfDownloadsForUpdatingProject(ProjectDataTemplate projectDataTemplate)
    {
        try
        {
            noOfDownloads = 0;
            downloadProgress = 0;

            if (!string.IsNullOrEmpty(projectDataTemplate.data.icon))
                noOfDownloads += 1; // project icon

            foreach (var version in projectDataTemplate.data.version.data)
            {
                bool findSubProject = (DataManager.instance.projects
                                           .Find(x => x.projectID.Equals(projectDataTemplate.data.barcode))
                                           .variants.Find(x => x.projectID.Equals(version.unique_id)) != null);

                if (!findSubProject)
                {
                    if (!string.IsNullOrEmpty(version.icon))
                        noOfDownloads += 1; //sub project icon

                    if (!string.IsNullOrEmpty(version.hot_spot_icon))
                        noOfDownloads += 1;//sub project hotspot icon

                    noOfDownloads += version.media.data.Count; //sub project panos

                    if (version.map != null && !string.IsNullOrEmpty(version.map.data.url))
                        noOfDownloads += 1; //sub project map
                }
            }
            Debug.Log("Total no. of downloads : " + noOfDownloads);
        }
        catch
        {
        }
    }

    public void CalculateNoOfDownloadsForUpdatingSubProject(SubProjectData subProjectData)
    {
        try
        {
            noOfDownloads = 0;
            downloadProgress = 0;

            if (!string.IsNullOrEmpty(subProjectData.icon))
                noOfDownloads += 1; // project icon

            if (!string.IsNullOrEmpty(subProjectData.hot_spot_icon))
                noOfDownloads += 1;//sub project hotspot icon

            noOfDownloads += subProjectData.media.data.Count;//sub project panos

            if (subProjectData.map != null && !string.IsNullOrEmpty(subProjectData.map.data.url))
                noOfDownloads += 1;//sub project map

            Debug.Log("Total no. of downloads : " + noOfDownloads);
        }
        catch
        {
        }
    }

    public void UpdateProgressOnDownload(int no)
    {
        noOfDownloaded += no;
        if (noOfDownloads > 0)
            downloadProgress = (noOfDownloaded / noOfDownloads) * 100f;
        Debug.Log("Download Progress : " + downloadProgress);
    }

    public void ResetDownloadProgress()
    {
        noOfDownloaded = 0;
        downloadProgress = 0;
        Debug.Log("Download Progress Reset");
    }

    public float CalculateProjectSize(string projectDataString)
    {
        float size = 0;
        ProjectDataTemplate projectDataTemplate = JsonConvert.DeserializeObject<ProjectDataTemplate>(projectDataString);
        foreach (var version in projectDataTemplate.data.version.data)
        {
            size += version.mediaFullSize;
        }
        return size;
    }

    public void DeleteProject(string projectID)
    {
        /*//test scenario - just delete entries
        downloadedProjectIDs.Remove(projectID);
        int tempIndex = -1;
        tempIndex = projects.FindIndex(x => x.projectID == projectID);
        if (tempIndex >= 0)
        {
            projects.RemoveAt(tempIndex);
        }
        return;*/
        string path = GetProjectDataDetailsFolderPath(ProjectType.Normal);
        if (Directory.Exists(path))
        {
            path = Path.Combine(path, projectID);
            if (File.Exists(path))
            {
                var info = new FileInfo(path);
                info.Delete();
                downloadedProjectIDs.Remove(projectID);
            }
        }

        Debug.Log("Project Details Deleted.");

        path = GetProjectDataFolderPath(ProjectType.Normal);
        path = Path.Combine(path, projectID);
        if (Directory.Exists(path))
        {
            var info = new DirectoryInfo(path);
            info.Delete(true);
            int index = -1;
            index = projects.FindIndex(x => x.projectID == projectID);
            if (index >= 0)
            {
                projects.RemoveAt(index);
            }
        }

        Debug.Log("Project Data Deleted.");
    }
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    void OnProjectOpened()
    {
        LoadSubProjectDataFromLocal(AppManager.instance.currentSelectedProjectType);
        EventManager.InvokeCurrentProjectDataLoadedFromLocal();
    }
    #endregion
}