﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Newtonsoft.Json;
using UnityEngine.iOS;
using UnityEngine.SceneManagement;
using UnityEngine.VR;

public class AppManager : MonoBehaviour
{
    public static AppManager instance;

    #region Public_Variables
    public bool logEnabled = false;
    [Space]
    public AppMode appMode;
    public string WebPage_URL = "https://Schaefer-shop.de/";
    [Header("Server Base URLs")]
    [Space]
    public string Test_Base_URL = "http://test-schaefer.maldechavda.me/api/";
    public string Production_Base_URL = "https://real-vision.schaefer-shop.de/api/";
    [Space]
    public string CurrentProjectID_Selected = "";
    public string CurrentSubProjectID_Selected = "";
    public string CurrentProjectID_Download = "";
    public string Download_PIN = "";
    public string Update_PIN = "";
    public bool escapeKeyEnable;
    public ProjectType currentSelectedProjectType;
    public bool projectOpened;

    [Space]
    [Header("Demo Projects")]
    public List<string> demoProjectIDs = new List<string>() { "149857027828" };
    public List<ProjectUpdateDetails> demoProjectsVersionDetails = new List<ProjectUpdateDetails>();
#if GVR
    [Space]
    [Header("GVR")]
    public GvrViewer gvrViewer;
    //    public GvrPointerInputModule gvrPointerInputModule;
    public GazeInputManager gazeInputManager;
#endif
#if OVR
    [Space]
    [Header("OVR")]
    public Transform ovrCameraRig;
#endif
    [Space]
    public Transform panoramaSphere;

    [Space]
    public bool memoryWarningDetected = false;
    #endregion

    #region Private_Variables
    #endregion

    #region Events
    public delegate void SystemEscapePressed();
    public static event SystemEscapePressed OnSystemEscapePressed;

    static void InvokeOnSystemEscapePressed()
    {
        var handler = OnSystemEscapePressed;
        if (handler != null) handler();
    }
    #endregion

    #region Unity_CallBacks

    void Awake()
    {
        instance = this;
#if UNITY_EDITOR
        Debug.logger.logEnabled = true;
#else
        Debug.logger.logEnabled = logEnabled;
#endif
        Debug.Log("SystemInfo.maxTextureSize : " + SystemInfo.maxTextureSize);
    }

    void OnEnable()
    {
        EventManager.CurrentProjectDataLoadedFromLocal += OnCurrentProjectDataLoadedFromLocal;
        EventManager.ProjectClosed += OnProjectClosed;
        Application.lowMemory += OnLowMemoryCallback;
    }

    // Use this for initialization
    IEnumerator Start()
    {
        DisableKeyboard();
        ToggleGazeInput(false);
        MoveStreamingAssetsToPersistantData();
        yield return new WaitForSeconds(0.1f);
        ToggleVRMode(false);
        VRSettings.enabled = false;
    }

    void Update()
    {
#if OVR
        if (escapeKeyEnable && Input.GetKeyDown(KeyCode.Escape))
        {
            InvokeOnSystemEscapePressed();
        }
#endif

#if GVR
        if (escapeKeyEnable && Input.GetKeyDown(KeyCode.Escape) && !projectOpened)
        {
            InvokeOnSystemEscapePressed();
        }

        if (escapeKeyEnable && GvrViewer.Instance.BackButtonPressed && projectOpened)
        {
            InvokeOnSystemEscapePressed();
        }
#endif

#if UNITY_EDITOR && GVR
        if (Input.GetKeyDown(KeyCode.T))
            ToggleVRMode(!gvrViewer.VRModeEnabled);
#endif
    }

    void OnDisable()
    {
        EventManager.CurrentProjectDataLoadedFromLocal -= OnCurrentProjectDataLoadedFromLocal;
        EventManager.ProjectClosed -= OnProjectClosed;
        Application.lowMemory -= OnLowMemoryCallback;
    }

    /*void OnGUI()
    {
        if (appMode.Equals(AppMode.Production) && GUI.Button(new Rect(0, Screen.height - 100, 100, 100), "Load Scene"))
        {
            SceneManager.LoadScene(0);
        }
    }*/
    #endregion

    #region Private_Methods

    void DisableKeyboard()
    {
#if OVR && !GVR
        TouchScreenKeyboard.hideInput = true;
#endif
    }
    #endregion

    #region Public_Methods
    public void MoveStreamingAssetsToPersistantData()
    {
#if UNITY_ANDROID
        if (UniAndroidPermission.IsPermitted(AndroidPermission.WRITE_EXTERNAL_STORAGE))
        {
            StartCoroutine(MoveStreamingAssetsToPersistantDataEnumerator());
        }
#elif UNITY_IOS
        StartCoroutine(MoveStreamingAssetsToPersistantDataEnumerator());
#endif
    }

    public string GetRootProjectID(string version)
    {
        string root = "";
        foreach (var temp in DataManager.instance.projects)
        {
            if (temp.variants.Count > 0)
            {
                foreach (var temp2 in temp.variants)
                {
                    if (temp2.projectID.Equals(version))
                    {
                        root = temp.projectID;
                        return root;
                    }
                }
            }
        }
        return root;
    }

    public void ToggleEscapeKey(bool enable)
    {
        escapeKeyEnable = enable;
    }

    public void RecenterVRViewer()
    {
#if GVR
        gvrViewer.Recenter();
#endif
#if OVR
#if UNITY_EDITOR
        ovrCameraRig.eulerAngles = Vector3.zero;
#else
        OVRManager.display.RecenterPose();
#endif
#endif
    }

    public void ToggleVRMode(bool enable)
    {
#if GVR
        if (IsIPad())
        {
            gvrViewer.VRModeEnabled = false;
            UIManager.instance.SetNormalModeForIPad();
            return;
        }

        gvrViewer.VRModeEnabled = enable;
        UIManager.instance.ToggleModesBackButton(!enable);
#endif
    }

    public void ToggleGazeInput(bool enable)
    {
#if GVR
        //        gvrPointerInputModule.enabled = enable;
        gazeInputManager.enabled = enable;
#endif
    }

    public void DownloadProject()
    {
#if UNITY_ANDROID
        if (UniAndroidPermission.IsPermitted(AndroidPermission.WRITE_EXTERNAL_STORAGE))
        {
            APIManager.instance.DownloadProject(CurrentProjectID_Download);
        }
        else
        {
            UniAndroidPermission.RequestPremission(AndroidPermission.WRITE_EXTERNAL_STORAGE, () =>
            {
                Debug.Log("Storage Permission Enabled Now.");// not getting this callback so manually check permission in coroutine
            });
            StartCoroutine(CheckStoragePermission(() =>
            {
                DownloadProject();
            }));
        }
#elif UNITY_IOS
        APIManager.instance.DownloadProject(CurrentProjectID_Download);
#endif
    }

    public void ProjectSelected(string projectID, ProjectType projectType)
    {
        UIManager.instance.uiFadeInOut.StartFadeIn(0.35f, () =>
        {
            if (projectID != CurrentSubProjectID_Selected)
            {
                PanoramaController.instance.ClearCachedPanoramas();
            }

            currentSelectedProjectType = projectType;
            CurrentSubProjectID_Selected = projectID;

            Debug.Log("CurrentSubProjectID_Selected : " + CurrentSubProjectID_Selected);
            EventManager.InvokeProjectOpened();
            UIManager.instance.uiFadeInOut.StartFadeOut(0.35f, null);
        });
    }

    public bool IsProjectDownloaded(string projectID)
    {
        return DataManager.instance.downloadedProjectIDs.Contains(projectID);
    }

    public void LoadPanoramaDataBasedOnPanoId(int panoId)
    {
        int panoIndex = DataManager.instance.currentSelectedSubProjectData.media.data.FindIndex(x => x.id.Equals(panoId));
        if (panoIndex >= 0 && panoIndex < DataManager.instance.currentSelectedSubProjectData.media.data.Count)
        {
            PanoramaData panoramaData = DataManager.instance.currentSelectedSubProjectData.media.data[panoIndex];
            PanoramaController.instance.LoadPanoramaFromLocal(panoId.ToString(), panoramaData.transmission);
            HotSpotsController.instance.ShowHotSpotsBasedOnCurrentPano(panoramaData);
            RecenterVRViewer();
            panoramaSphere.localEulerAngles = new Vector3(/*panoramaData.y*/0, -panoramaData.x, 0);
        }
    }

    public bool IsUpdateAvailableForProject(string projectID)
    {
        ProjectDetailsTemplate projectDetails = DataManager.instance.projects.Find(x => x.projectID.Equals(projectID));
        ProjectUpdateDetails projectUpdateDetails = DataManager.instance.updateAvailableForProjects.Find(x => x.projectID.Equals(projectID));

        if (projectUpdateDetails == null)
            return false;

        //for checking any newly added project
        foreach (var updateSubProjects in projectUpdateDetails.subProjects)
        {
            if (projectDetails.variants.Find(x => x.projectID.Equals(updateSubProjects.unique_id)) == null)
            {
                return true;
            }
        }

        //for checking any deleted project
        foreach (var subProjects in projectDetails.variants)
        {
            if (projectUpdateDetails.subProjects.Find(x => x.unique_id.Equals(subProjects.projectID)) == null)
            {
                return true;
            }
        }
        return false;
    }

    public bool IsUpdateAvailableForSubProject(string subProjectID)
    {
        bool updateAvailable = false;
        foreach (var projects in DataManager.instance.updateAvailableForProjects)
        {
            SubProjectUpdateDetails sub = projects.subProjects.Find(x => x.unique_id == subProjectID);
            if (sub != null)
            {
                float versionNumber = DataManager.instance.projects
                    .Find(x => x.projectID.Equals(GetRootProjectID(subProjectID)))
                    .variants.Find(x => x.projectID.Equals(subProjectID))
                    .currentProjectVersion;
                updateAvailable = (sub.currentProjectVersion > versionNumber);
                break;
            }
        }
        return updateAvailable;
    }

    public void UpdateProject(string projectID)
    {
        APIManager.instance.UpdateProject(projectID);
    }

    public void UpdateSubProject(string projectID)
    {
        Debug.Log("> Update Sub Project : " + projectID);
        APIManager.instance.UpdateSubProject(projectID, GetRootProjectID(projectID));
    }

    public void CloseProject()
    {
        UIManager.instance.uiFadeInOut.StartFadeIn(0.35f, () =>
        {
            ToggleVRMode(false);
            ToggleGazeInput(false);
            OnSystemEscapePressed -= CloseProject;
            EventManager.InvokeProjectClosed();
            projectOpened = false;
            RecenterVRViewer();
            UIManager.instance.uiFadeInOut.StartFadeOut(0.35f, null);
        });
    }

    public void SetSubProjectHierarchy(string hierarchy, ProjectType projectType)
    {
        List<ProjectDetailsTemplate> projects;
        if (projectType.Equals(ProjectType.Normal))
        {
            projects = DataManager.instance.projects;
        }
        else
        {
            projects = DataManager.instance.demoProjects;
        }
        foreach (var temp in projects)
        {
            if (temp.variants.Count > 0)
            {
                foreach (var temp2 in temp.variants)
                {
                    CurrentSubProjectID_Selected = Path.Combine(temp.projectID, temp2.projectID);
                    if (temp2.projectID.Equals(hierarchy))
                        return;
                }
            }
        }
    }

#if UNITY_IOS
    [DllImport("__Internal")]
    static extern int HWUtils_getFreeMemory();

    public static int GetFreeMemory()
    {
#if UNITY_EDITOR
        return int.MaxValue;
#else
        return HWUtils_getFreeMemory();
#endif
    }
#endif

    public bool IsIPad()
    {
#if UNITY_IOS
        return SystemInfo.deviceModel.Contains("iPad");
#endif
        return false;
    }

    public bool NeedToCompressImage()
    {
        bool needToCompress = false;
#if UNITY_IOS
        if (Device.generation == DeviceGeneration.iPhone || Device.generation == DeviceGeneration.iPhone3GS
            || Device.generation == DeviceGeneration.iPhone4 || IsIPad())
        {
            needToCompress = true;
        }
#endif
        return needToCompress;
    }
    #endregion

    #region Coroutines
    IEnumerator MoveStreamingAssetsToPersistantDataEnumerator()
    {
        string destination = Path.Combine(Application.persistentDataPath, Constants.DemoProjects_Folder_Name);
        string destinationDataDetailsPath = Path.Combine(destination, Constants.ProjectDataDetails_Folder_Name);
        string destinationDataPath = Path.Combine(destination, Constants.ProjectData_Folder_Name);

        string source = Constants.SAPath;
        string sorceDataDetailsPath = Path.Combine(source, Constants.ProjectDataDetails_Folder_Name);
        string sourceDataPath = Path.Combine(source, Constants.ProjectData_Folder_Name);

        bool needToMove = false;

        foreach (var demoProjectID in demoProjectIDs)
        {
            needToMove = false;
            string fileName = Path.Combine(sorceDataDetailsPath, demoProjectID);
            WWW www = new WWW(fileName);
            yield return www;

            ProjectDataTemplate newData = null;
            if (www.text != null)
            {
                newData = JsonConvert.DeserializeObject<ProjectDataTemplate>(www.text.ToDecodedString());
                string oldfileName = Path.Combine(destinationDataDetailsPath, demoProjectID);

                ProjectUpdateDetails projectUpdateDetails =
                    demoProjectsVersionDetails.Find(x => x.projectID.Equals(demoProjectID));
                if (projectUpdateDetails != null)
                {
                    for (int i = 0; i < newData.data.version.data.Count; i++)
                    {
                        string versionID = newData.data.version.data[i].unique_id;
                        SubProjectUpdateDetails subProjectUpdateDetails =
                            projectUpdateDetails.subProjects.Find(x => x.unique_id.Equals(versionID));
                        if (subProjectUpdateDetails != null)
                            newData.data.version.data[i].currentProjectVersion = subProjectUpdateDetails.currentProjectVersion;
                    }
                }

                if (File.Exists(oldfileName))
                {
                    string projData = File.ReadAllText(oldfileName);
                    projData = projData.ToDecodedString();
                    ProjectDataTemplate oldData =
                        JsonConvert.DeserializeObject<ProjectDataTemplate>(projData);

                    //for checking any newly added project
                    foreach (var updateSubProjects in newData.data.version.data)
                    {
                        var version = oldData.data.version.data.Find(x => x.unique_id.Equals(updateSubProjects.unique_id));
                        if (version == null)
                        {
                            needToMove = true;
                            break;
                        }
                        else if (version.currentProjectVersion != updateSubProjects.currentProjectVersion)
                        {
                            needToMove = true;
                            break;
                        }
                    }
                }
                else
                {
                    needToMove = true;
                }
            }
            if (string.IsNullOrEmpty(www.error) && needToMove && newData != null)
            {
                if (!Directory.Exists(destinationDataDetailsPath))
                {
                    Directory.CreateDirectory(destinationDataDetailsPath);
                }
                byte[] bytesData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(newData).ToEncodedString());
                // make sure the destination directory exists
                File.WriteAllBytes(Path.Combine(destinationDataDetailsPath, demoProjectID), bytesData);
            }

            //            foreach (var demoProjectID in demoProjectIDs)
            if (needToMove)
            {
                string msg = LocalizationManager.instance.GetLocalizedValue("Generating Demo Projects");
                LoadingPanel.instance.ShowLoadingPanel(msg, null);

                Debug.Log("Moving Project From StreamingAssets To PersistantData : " + demoProjectID);
                string fileNameTemp = Path.Combine(destinationDataDetailsPath, demoProjectID);

                if (File.Exists(fileNameTemp))
                {
                    string projData = File.ReadAllText(fileNameTemp);
                    projData = projData.ToDecodedString();
                    ProjectDataTemplate currentSelectedProjectData =
                        JsonConvert.DeserializeObject<ProjectDataTemplate>(projData);

                    string sourceProjectPath = Path.Combine(sourceDataPath, currentSelectedProjectData.data.barcode);
                    string destProjectPath = Path.Combine(destinationDataPath, currentSelectedProjectData.data.barcode);

                    if (Directory.Exists(destProjectPath))
                    {
                        var info = new DirectoryInfo(destProjectPath);
                        info.Delete(true);
                    }

                    if (!Directory.Exists(destProjectPath))
                    {
                        Directory.CreateDirectory(destProjectPath);
                    }

                    string projIconPath = Path.Combine(sourceProjectPath, Constants.Project_Icon_Image_Name + Constants.PNG_Ext);

                    WWW projIcon = new WWW(projIconPath);
                    yield return projIcon;

                    if (string.IsNullOrEmpty(projIcon.error))
                    {
                        File.WriteAllBytes(Path.Combine(destProjectPath,
                            Constants.Project_Icon_Image_Name + Constants.PNG_Ext), projIcon.bytes);
                    }
                    projIcon.Dispose();

                    foreach (var subProjectData in currentSelectedProjectData.data.version.data)
                    {
                        string sourcePanoramaPath = Path.Combine(sourceProjectPath, subProjectData.unique_id);
                        sourcePanoramaPath = Path.Combine(sourcePanoramaPath, Constants.Panorama_Folder_Name);

                        string destPanoramaPath = Path.Combine(destProjectPath, subProjectData.unique_id);
                        destPanoramaPath = Path.Combine(destPanoramaPath, Constants.Panorama_Folder_Name);

                        if (!Directory.Exists(destPanoramaPath))
                        {
                            Directory.CreateDirectory(destPanoramaPath);
                        }

                        foreach (var pano in subProjectData.media.data)
                        {
                            string panorama = Path.Combine(sourcePanoramaPath, pano.id + Constants.PNG_Ext);

                            WWW wwwPano = new WWW(panorama);
                            yield return wwwPano;

                            Texture2D texturePano = wwwPano.texture;
                            /*Texture2D texturePano = new Texture2D(1, 1);
                            wwwPano.LoadImageIntoTexture(texturePano);*/

                            if (AppManager.instance.NeedToCompressImage())
                            {
                                texturePano = Utilities.CompressTexture(texturePano);
                            }

                            if (string.IsNullOrEmpty(wwwPano.error))
                            {
                                Utilities.SaveTextureAsPNG(texturePano, Path.Combine(destPanoramaPath, pano.id + Constants.PNG_Ext));
                                //                                File.WriteAllBytes(Path.Combine(destPanoramaPath, pano.id + Constants.PNG_Ext), wwwPano.bytes);
                            }
                            Destroy(texturePano);
                            wwwPano.Dispose();
                            wwwPano = null;
                        }

                        string map = Path.Combine(Path.Combine(sourceProjectPath, subProjectData.unique_id), Constants.Map_Image_Name + Constants.PNG_Ext);

                        WWW mapwww = new WWW(map);
                        yield return mapwww;

                        Texture2D textureMap = mapwww.texture;
                        /*Texture2D textureMap = new Texture2D(1, 1);
                        mapwww.LoadImageIntoTexture(textureMap);*/

                        if (AppManager.instance.NeedToCompressImage())
                        {
                            textureMap = Utilities.CompressTexture(textureMap);
                        }

                        if (string.IsNullOrEmpty(mapwww.error))
                        {
                            Utilities.SaveTextureAsPNG(textureMap, Path.Combine(Path.Combine(destProjectPath, subProjectData.unique_id),
                                Constants.Map_Image_Name + Constants.PNG_Ext));
                            /*File.WriteAllBytes(Path.Combine(Path.Combine(destProjectPath, subProjectData.unique_id),
                                Constants.Map_Image_Name + Constants.PNG_Ext), mapwww.bytes);*/
                        }
                        Destroy(textureMap);
                        mapwww.Dispose();
                        mapwww = null;

                        string icon = Path.Combine(Path.Combine(sourceProjectPath, subProjectData.unique_id), Constants.Project_Icon_Image_Name + Constants.PNG_Ext);

                        WWW iconwww = new WWW(icon);
                        yield return iconwww;

                        if (string.IsNullOrEmpty(iconwww.error))
                        {
                            File.WriteAllBytes(Path.Combine(Path.Combine(destProjectPath, subProjectData.unique_id),
                                Constants.Project_Icon_Image_Name + Constants.PNG_Ext), iconwww.bytes);
                        }
                        iconwww.Dispose();

                        string hotSpotIcon = Path.Combine(Path.Combine(sourceProjectPath, subProjectData.unique_id), Constants.HotSpot_Icon_Image_Name + Constants.PNG_Ext);

                        WWW hotSpotIconWww = new WWW(hotSpotIcon);
                        yield return hotSpotIconWww;

                        if (string.IsNullOrEmpty(hotSpotIconWww.error))
                        {
                            File.WriteAllBytes(Path.Combine(Path.Combine(destProjectPath, subProjectData.unique_id),
                                Constants.HotSpot_Icon_Image_Name + Constants.PNG_Ext), hotSpotIconWww.bytes);
                        }
                        hotSpotIconWww.Dispose();
                    }
                }
                LoadingPanel.instance.HideLoadingPanel();
            }
        }
        Resources.UnloadUnusedAssets();
        GC.Collect();
        EventManager.InvokeMoveFromSAToPDComplete();
        Debug.Log("Moving Projects From StreamingAssets To PersistantData Completed");
    }

#if UNITY_ANDROID
    IEnumerator CheckStoragePermission(Action action)
    {
        yield return new WaitForSeconds(0.1f);
        if (UniAndroidPermission.IsPermitted(AndroidPermission.WRITE_EXTERNAL_STORAGE))
        {
            Debug.Log("Storage Permission");
            if (action != null)
                action.Invoke();
        }
    }
#endif
    #endregion

    #region Custom_CallBacks
    void OnCurrentProjectDataLoadedFromLocal()
    {
        try
        {
            int initialIndex = DataManager.instance.currentSelectedSubProjectData.media.data.FindIndex(x => x.is_start_point);
            int panoId;
            if (initialIndex < 0)
            {
                panoId = DataManager.instance.currentSelectedSubProjectData.media.data[0].id;
            }
            else
            {
                panoId = DataManager.instance.currentSelectedSubProjectData.media.data[initialIndex].id;
            }
            PanoramaController.instance.TogglePanoramaSphere(true);
            LoadPanoramaDataBasedOnPanoId(panoId);
            ToggleVRMode(true);
            ToggleGazeInput(true);
            projectOpened = true;
            OnSystemEscapePressed += CloseProject;
            UIManager.instance.uiFadeInOut.StartFadeOut(0.35f, null);
        }
        catch
        {
            CloseProject();
        }
    }

    void OnProjectClosed()
    {
        ToggleVRMode(false);
    }

    void OnLowMemoryCallback()
    {
        memoryWarningDetected = true;
        Debug.Log("Memory Warning Detected");
    }
    #endregion
}
