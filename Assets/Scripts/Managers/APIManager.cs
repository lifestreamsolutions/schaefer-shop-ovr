﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class APIManager : MonoBehaviour
{
    public static APIManager instance;
    #region Public_Variables
    #endregion

    #region Private_Variables
    string _currentProjectID_BeingDownload = "";
    string _currentSubProjectID_BeingDownload = "";

    bool _errorWhileDownloading = false;
    CoroutineController _downloadCoroutine;
    bool _downloadingCanceledManually = false;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks
    void Awake()
    {
        instance = this;
    }

    void OnEnable() { }

    // Use this for initialization
    void Start() { }

    void OnDisable()
    {
    }
    #endregion

    #region Private_Methods
    Dictionary<string, string> GetAPIHeader()
    {
        Dictionary<string, string> headers = new Dictionary<string, string>();
        //        headers.Add("Authorization", "Bearer " + DataManager.Instance.LoginData.api_token);
        return headers;
    }

    string GetPanoramaFolderPath(ProjectType projectType)
    {
        string basePath = "";
        if (projectType.Equals(ProjectType.Demo))
        {
            basePath = Path.Combine(Application.persistentDataPath, Constants.DemoProjects_Folder_Name);
        }
        else
        {
            basePath = Application.persistentDataPath;
        }
        string folderPath = Path.Combine(Path.Combine(basePath, Constants.ProjectData_Folder_Name),
            Path.Combine(Path.Combine(_currentProjectID_BeingDownload, _currentSubProjectID_BeingDownload), Constants.Panorama_Folder_Name));
        return folderPath;
    }

    void ToggleCheckErrorWhileDownloading(bool start)
    {
        StopCoroutine("TrackingErrorWhileDownloading");
        if (start)
        {
            StartCoroutine("TrackingErrorWhileDownloading");
        }
    }

    void StopDownloadCoroutines()
    {
        _downloadCoroutine.Stop();
        _downloadCoroutine.StopNestedCoroutines();
    }

    void CancelDownloading()
    {
        Debug.Log("Cancel Downloading");
        _downloadingCanceledManually = true;
        StopDownloadCoroutines();
    }

    void ToggleEscapeFunctionality(bool enable)
    {
        AppManager.instance.ToggleEscapeKey(enable);
    }

    void ShowDownloadSuccessError(string titleKey, JObject jo)
    {
        string title = LocalizationManager.instance.GetLocalizedValue(titleKey);
        string message = "";

        if (jo[Constants.EN_Message_Key] != null && LocalizationManager.instance.SelectedLanguageIndex == 0)
        {
            message = jo[Constants.EN_Message_Key].ToString();
        }
        else if (jo[Constants.DE_Message_Key] != null && LocalizationManager.instance.SelectedLanguageIndex == 1)
        {
            message = jo[Constants.DE_Message_Key].ToString();
        }
        else if (jo[Constants.FR_Message_Key] != null && LocalizationManager.instance.SelectedLanguageIndex == 2)
        {
            message = jo[Constants.FR_Message_Key].ToString();
        }
        else
        {
            message = jo[Constants.Message_Key].ToString();
        }

        MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, title, message, null, null, Constants.Ok);
    }

    void ShowNoEnoughMemoryMessage()
    {
        string title = LocalizationManager.instance.GetLocalizedValue("Download Project");
        string message = LocalizationManager.instance.GetLocalizedValue("Not Enough Memory Message");
        /*
                if (jo[Constants.EN_Message_Key] != null && LocalizationManager.instance.SelectedLanguageIndex == 0)
                {
                    message = jo[Constants.EN_Message_Key].ToString();
                }
                else if (jo[Constants.DE_Message_Key] != null && LocalizationManager.instance.SelectedLanguageIndex == 1)
                {
                    message = jo[Constants.DE_Message_Key].ToString();
                }
                else
                {
                    message = jo[Constants.Message_Key].ToString();
                }
        */
        MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, title, message, null, null, Constants.Ok);
        Debug.Log("--- Not Enough Memory ---");
    }
    #endregion

    #region Public_Methods

    public void DownloadProject(string projectID)
    {
        DataManager.instance.ResetDownloadProgress();
        ToggleEscapeFunctionality(false);
        _errorWhileDownloading = false;
        _currentProjectID_BeingDownload = projectID;
        string msg = LocalizationManager.instance.GetLocalizedValue("Downloading Project");
        LoadingPanel.instance.ShowLoadingPanel(msg, CancelDownloading);
        this.StartCoroutineEx(DownloadProjectDetailsEnumerator(), out _downloadCoroutine);
        _downloadCoroutine.Start();
    }

    public void CheckUpdateAvailabilityForProjects()
    {
        Debug.Log("--- Checking Project Update ---");
        foreach (var project in DataManager.instance.projects)
        {
            StartCoroutine(ProjectUpdateAvailabilityEnumerator(project.projectID));
        }
    }

    public void UpdateProject(string projectID)
    {
        DataManager.instance.ResetDownloadProgress();
        ToggleEscapeFunctionality(false);
        string msg = LocalizationManager.instance.GetLocalizedValue("Updating Project");
        LoadingPanel.instance.ShowLoadingPanel(msg, CancelDownloading);
        _errorWhileDownloading = false;
        _currentProjectID_BeingDownload = projectID;

        this.StartCoroutineEx(UpdateProjectEnumerator(), out _downloadCoroutine);
        _downloadCoroutine.Start();
    }

    public void UpdateSubProject(string subProject, string projectID)
    {
        DataManager.instance.ResetDownloadProgress();
        ToggleEscapeFunctionality(false);
        string msg = LocalizationManager.instance.GetLocalizedValue("Updating Project");
        LoadingPanel.instance.ShowLoadingPanel(msg, CancelDownloading);
        _errorWhileDownloading = false;
        _currentProjectID_BeingDownload = projectID;

        this.StartCoroutineEx(UpdateSubProjectEnumerator(subProject), out _downloadCoroutine);
        _downloadCoroutine.Start();
    }
    #endregion

    #region Coroutines

    IEnumerator TrackingErrorWhileDownloading()
    {
        while (true)
        {
            if (_downloadingCanceledManually)
            {
                _downloadingCanceledManually = false;
                UIManager.instance.ToggleDownloadProgressPanel(false);
                ToggleEscapeFunctionality(true);
                yield break;
            }
            if (_errorWhileDownloading)
            {
                Debug.Log("Error occured while downloading");
                LoadingPanel.instance.HideLoadingPanel();

                string dlFailed = LocalizationManager.instance.GetLocalizedValue("Download Failed");
                string dlFailedMessage = LocalizationManager.instance.GetLocalizedValue("Project Download Error Message");
                dlFailedMessage = dlFailedMessage.Replace("\\n", "\n");

                MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Simple, dlFailed, dlFailedMessage, null, null, Constants.Ok);
                StopDownloadCoroutines();
                UIManager.instance.ToggleDownloadProgressPanel(false);
                ToggleEscapeFunctionality(true);
                yield break;
            }
            yield return null;
        }
    }

    IEnumerator DownloadProjectDetailsEnumerator()
    {
        ToggleCheckErrorWhileDownloading(start: true);

        WWWForm dataForm = new WWWForm();
        dataForm.AddField("barcode", _currentProjectID_BeingDownload);
        dataForm.AddField("pin", AppManager.instance.Download_PIN);

        //        WWW downloadProject = new WWW(Constants.Download_Project_URL, dataForm);
        WWW downloadProject = new WWW(Constants.Download_Project_Barcode_URL, dataForm);
        yield return downloadProject;

        if (downloadProject.error == null)
        {
            //            print(downloadProject.text);
            JObject jo = JObject.Parse(downloadProject.text);
            bool success;
            if (jo[Constants.Data_Key] != null)
            {
                success = true;
            }
            else
            {
                success = (bool)jo[Constants.Success_Key];
            }

            if (success)
            {
                Debug.Log("Project Details Download : Success");
                bool enoughMemory = true;
#if UNITY_IOS
                float targetSize = DataManager.instance.CalculateProjectSize(downloadProject.text);
                Debug.Log("Required Memory : " + targetSize);
                Debug.Log("Free Memory : " + (AppManager.GetFreeMemory() / (1024 * 1024)));
                enoughMemory = ((AppManager.GetFreeMemory() / (1024 * 1024)) >= targetSize);
#endif
                if (enoughMemory)
                {
                    yield return _downloadCoroutine.StartNestedCoroutines(DownloadProjectDataEnumerator(downloadProject.text));
                    yield return new WaitForSeconds(3); //wait to update progress in UI
                    if (!_errorWhileDownloading && !_downloadingCanceledManually)
                    {
                        DataManager.instance.ProjectDataDownloaded(_currentProjectID_BeingDownload, downloadProject.text);
                        Debug.Log("--- Project DATA Downloaded ---");
                    }
                }
                else
                {
                    ShowNoEnoughMemoryMessage();
                }

                ToggleCheckErrorWhileDownloading(start: false);
            }
            else
            {
                ShowDownloadSuccessError("Download Project", jo);
            }

            ToggleEscapeFunctionality(true);
            UIManager.instance.ToggleDownloadProgressPanel(false);
        }
        else
        {
            Debug.Log("Project Details Download : Failed\nError : " + downloadProject.error);
            _errorWhileDownloading = true;
        }
        LoadingPanel.instance.HideLoadingPanel();
    }

    IEnumerator DownloadProjectDataEnumerator(string projectDataString)
    {
        Debug.Log("> Downloading Project Data");
        ProjectDataTemplate projectDataTemplate = JsonConvert.DeserializeObject<ProjectDataTemplate>(projectDataString);

        DataManager.instance.CalculateNoOfDownloadsForProjectDownload(projectDataTemplate);
        UIManager.instance.ToggleDownloadProgressPanel(true);

        yield return _downloadCoroutine.StartNestedCoroutines(DownloadProjectIcon(projectDataTemplate.data.icon));

        foreach (var subProjectData in projectDataTemplate.data.version.data)
        {
            Debug.Log("-> Downloading Sub Project Data : " + subProjectData.unique_id);
            _currentSubProjectID_BeingDownload = subProjectData.unique_id;
            yield return _downloadCoroutine.StartNestedCoroutines(DownloadSubProjectDataEnumerator(subProjectData));
        }
    }

    IEnumerator DownloadSubProjectDataEnumerator(SubProjectData subProjectData)
    {
        Debug.Log("--> Downloading SubProject Icon");
        yield return _downloadCoroutine.StartNestedCoroutines(DownloadSubProjectIcon(subProjectData.icon));

        Debug.Log("--> Downloading SubProject HotSpot Icon");
        yield return _downloadCoroutine.StartNestedCoroutines(DownloadSubProjectHotSpotIcon(subProjectData.hot_spot_icon));

        Debug.Log("--> Downloading Map");
        yield return _downloadCoroutine.StartNestedCoroutines(DownloadMapData(subProjectData.map.data));

        Debug.Log("--> Downloading All Panoramas");
        foreach (var pano in subProjectData.media.data)
        {
            yield return _downloadCoroutine.StartNestedCoroutines(DownloadPanorama(pano));
        }
    }

    IEnumerator DownloadPanorama(PanoramaData data)
    {
        if (!string.IsNullOrEmpty(data.url))
        {
            WWW panorama = new WWW(data.url);
            yield return panorama;
            //            Texture2D panoramTexture2D = new Texture2D(1, 1);
            if (panorama.error == null)
            {
                Debug.Log("---> Panorama Downloaded : " + data.id);
                //                panorama.LoadImageIntoTexture(panoramTexture2D);
                DataManager.instance.SavePanorama(GetPanoramaFolderPath(ProjectType.Normal), panorama.texture, data.id);
                DataManager.instance.UpdateProgressOnDownload(1);
            }
            else
            {
                Debug.Log("---> Panorama Error : " + data.id + " _ " + panorama.error);
                _errorWhileDownloading = true;
            }
            //            Destroy(panoramTexture2D);
            panorama.Dispose();
            panorama = null;
        }
        yield return null;
        GC.Collect();
    }

    IEnumerator DownloadMapData(MapData data)
    {
        if (!string.IsNullOrEmpty(data.url))
        {
            WWW map = new WWW(data.url);
            yield return map;
            //            Texture2D mapTexture2D = new Texture2D(1, 1);
            if (map.error == null)
            {
                Debug.Log("---> Map Downloaded");
                //                map.LoadImageIntoTexture(mapTexture2D);
                string folderPath = Path.Combine(
                    Path.Combine(Application.persistentDataPath, Constants.ProjectData_Folder_Name),
                    Path.Combine(_currentProjectID_BeingDownload, _currentSubProjectID_BeingDownload));
                DataManager.instance.SaveMap(folderPath, map.texture);
                DataManager.instance.UpdateProgressOnDownload(1);
            }
            else
            {
                Debug.Log("---> Map Error : " + map.error);
                _errorWhileDownloading = true;
            }
            //            Destroy(mapTexture2D);
            map.Dispose();
            map = null;
        }
        yield return null;
        GC.Collect();
    }

    IEnumerator DownloadSubProjectIcon(string url)
    {
        if (!string.IsNullOrEmpty(url))
        {
            WWW projectIcon = new WWW(url);
            yield return projectIcon;

            if (projectIcon.error == null)
            {
                Debug.Log("---> SubProject Icon Downloaded : " + _currentSubProjectID_BeingDownload);
                string folderPath = Path.Combine(
                    Path.Combine(Application.persistentDataPath, Constants.ProjectData_Folder_Name),
                    Path.Combine(_currentProjectID_BeingDownload, _currentSubProjectID_BeingDownload));
                DataManager.instance.SaveProjectIcon(folderPath, projectIcon.texture);
                DataManager.instance.UpdateProgressOnDownload(1);
            }
            else
            {
                Debug.Log("---> SubProject Icon Error : " + _currentSubProjectID_BeingDownload + " _ " + projectIcon.error);
                _errorWhileDownloading = true;
            }
            projectIcon.Dispose();
            projectIcon = null;
        }
        yield return null;
        GC.Collect();
    }

    IEnumerator DownloadProjectIcon(string url)
    {
        if (!string.IsNullOrEmpty(url))
        {
            WWW projectIcon = new WWW(url);
            yield return projectIcon;

            if (projectIcon.error == null)
            {
                Debug.Log("---> Project Icon Downloaded : " + _currentProjectID_BeingDownload);
                string folderPath = Path.Combine(
                    Path.Combine(Application.persistentDataPath, Constants.ProjectData_Folder_Name),
                    _currentProjectID_BeingDownload);
                DataManager.instance.SaveProjectIcon(folderPath, projectIcon.texture);
                DataManager.instance.UpdateProgressOnDownload(1);
            }
            else
            {
                Debug.Log("---> Project Icon Error : " + _currentProjectID_BeingDownload + " _ " + projectIcon.error);
                _errorWhileDownloading = true;
            }
            projectIcon.Dispose();
            projectIcon = null;
        }
        yield return null;
        GC.Collect();
    }

    IEnumerator DownloadSubProjectHotSpotIcon(string url)
    {
        if (!string.IsNullOrEmpty(url))
        {
            WWW hotspotIcon = new WWW(url);
            yield return hotspotIcon;

            if (hotspotIcon.error == null)
            {
                Debug.Log("---> SubProject HotSpot Icon Downloaded : " + _currentSubProjectID_BeingDownload);
                string folderPath = Path.Combine(
                    Path.Combine(Application.persistentDataPath, Constants.ProjectData_Folder_Name),
                    Path.Combine(_currentProjectID_BeingDownload, _currentSubProjectID_BeingDownload));
                DataManager.instance.SaveSubProjectHotSpotIcon(folderPath, hotspotIcon.texture);
                DataManager.instance.UpdateProgressOnDownload(1);
            }
            else
            {
                Debug.Log("---> SubProject HotSpot Icon Error : " + _currentSubProjectID_BeingDownload + " _ " + hotspotIcon.error);
                _errorWhileDownloading = true;
            }
            hotspotIcon.Dispose();
            hotspotIcon = null;
        }
        yield return null;
        GC.Collect();
    }

    /*/// <summary>
    /// check update for project based on "ID"(NOT ProjectID/Barcode) of project
    /// </summary>
    /// <param name="projectId">barcode</param>
    /// <param name="id"></param>
    /// <returns></returns>
    IEnumerator UpdateAvailabilityEnumerator(string projectId, int id)
    {
        WWWForm dataForm = new WWWForm();
        dataForm.AddField("id", id.ToString());

        WWW projUpdate = new WWW(Constants.Check_Update_SubProject_URL, dataForm);
        yield return projUpdate;

        if (projUpdate.error == null)
        {
            JObject jo = JObject.Parse(projUpdate.text);
            bool success = (bool)jo[Constants.Success_Key];
            if (success)
            {
                int versionNumber = (int)jo[Constants.CurrentProjectVersion_Key];
                DataManager.instance.AddProjectToAvailableUpdateList(projectId, versionNumber);
            }
            else
            {
                Debug.Log("> Project Update NOT Available : " + projectId);
            }
        }
        else
        {
            Debug.Log("> Project Update Error : " + projectId + " _ " + projUpdate.error);
        }
    }*/

    /// <summary>
    /// check update for project based on ProjectID/Barcode of project
    /// </summary>
    /// <param name="projectId">barcode</param>
    /// <returns></returns>
    IEnumerator ProjectUpdateAvailabilityEnumerator(string projectId)
    {
        WWWForm dataForm = new WWWForm();
        dataForm.AddField("barcode", projectId);

        WWW projUpdate = new WWW(Constants.Check_Update_Project_URL, dataForm);
        yield return projUpdate;

        if (projUpdate.error == null)
        {
            ProjectUpdateDetailsTemplate projectUpdateDetailsTemplate =
                JsonConvert.DeserializeObject<ProjectUpdateDetailsTemplate>(projUpdate.text);

            if (projectUpdateDetailsTemplate.success)
            {
                DataManager.instance.AddProjectToAvailableUpdateList(projectId, projectUpdateDetailsTemplate.data);
            }
            else
            {
                Debug.Log("> Project Update NOT Available : " + projectId);
            }
        }
        else
        {
            Debug.Log("> Project Update Error : " + projectId + " _ " + projUpdate.error);
        }
        projUpdate.Dispose();
        projUpdate = null;
    }

    IEnumerator UpdateProjectEnumerator()
    {
        ToggleCheckErrorWhileDownloading(start: true);

        WWWForm dataForm = new WWWForm();
        dataForm.AddField("barcode", _currentProjectID_BeingDownload);
        dataForm.AddField("pin", AppManager.instance.Update_PIN);

        //        WWW downloadProject = new WWW(Constants.Download_Project_URL, dataForm);
        WWW downloadProject = new WWW(Constants.Download_Project_Barcode_URL, dataForm);
        yield return downloadProject;

        if (downloadProject.error == null)
        {
            JObject jo = JObject.Parse(downloadProject.text);
            bool success;
            if (jo[Constants.Data_Key] != null)
            {
                success = true;
            }
            else
            {
                success = (bool)jo[Constants.Success_Key];
            }

            if (success)
            {
                Debug.Log("Project Details Update : Success");
                yield return _downloadCoroutine.StartNestedCoroutines(UpdateProjectDataEnumerator(downloadProject.text));
                yield return new WaitForSeconds(3);
                if (!_errorWhileDownloading && !_downloadingCanceledManually)
                {
                    DataManager.instance.ProjectDataUpdated(_currentProjectID_BeingDownload, downloadProject.text);
                    Debug.Log("--- Project DATA Updated ---");

                    ToggleCheckErrorWhileDownloading(start: false);
                }
            }
            else
            {
                ShowDownloadSuccessError("Update Project", jo);
            }

            ToggleEscapeFunctionality(true);
            UIManager.instance.ToggleDownloadProgressPanel(false);
        }
        else
        {
            Debug.Log("Project Details Download : Failed\nError : " + downloadProject.error);
            _errorWhileDownloading = true;
        }
        LoadingPanel.instance.HideLoadingPanel();
        downloadProject.Dispose();
        downloadProject = null;
    }

    IEnumerator UpdateProjectDataEnumerator(string projectDataString)
    {
        Debug.Log("> Updating Project Data");
        ProjectDataTemplate projectDataTemplate = JsonConvert.DeserializeObject<ProjectDataTemplate>(projectDataString);

        yield return _downloadCoroutine.StartNestedCoroutines(DownloadProjectIcon(projectDataTemplate.data.icon));

        DataManager.instance.CalculateNoOfDownloadsForUpdatingProject(projectDataTemplate);
        UIManager.instance.ToggleDownloadProgressPanel(true);

        foreach (var subProjectData in projectDataTemplate.data.version.data)
        {
            bool findSubProject = (DataManager.instance.projects
                .Find(x => x.projectID.Equals(_currentProjectID_BeingDownload))
                .variants.Find(x => x.projectID.Equals(subProjectData.unique_id)) != null);

            if (!findSubProject)
            {
                Debug.Log("-> Adding Sub Project Data : " + subProjectData.unique_id);
                _currentSubProjectID_BeingDownload = subProjectData.unique_id;
                yield return _downloadCoroutine.StartNestedCoroutines(DownloadSubProjectDataEnumerator(subProjectData));
            }
        }
    }

    IEnumerator UpdateSubProjectEnumerator(string subProjectID)
    {
        ToggleCheckErrorWhileDownloading(start: true);

        WWWForm dataForm = new WWWForm();
        dataForm.AddField("barcode", _currentProjectID_BeingDownload);
        dataForm.AddField("pin", AppManager.instance.Update_PIN);

        //        WWW downloadProject = new WWW(Constants.Download_Project_URL, dataForm);
        WWW downloadProject = new WWW(Constants.Download_Project_Barcode_URL, dataForm);
        yield return downloadProject;

        if (downloadProject.error == null)
        {
            JObject jo = JObject.Parse(downloadProject.text);
            bool success;
            if (jo[Constants.Data_Key] != null)
            {
                success = true;
            }
            else
            {
                success = (bool)jo[Constants.Success_Key];
            }

            if (success)
            {
                Debug.Log("Project Details Update : Success");
                yield return _downloadCoroutine.StartNestedCoroutines(UpdateSubProjectDataEnumerator(downloadProject.text, subProjectID));
                yield return new WaitForSeconds(3);
                if (!_errorWhileDownloading && !_downloadingCanceledManually)
                {
                    DataManager.instance.SubProjectDataUpdated(_currentProjectID_BeingDownload, subProjectID, downloadProject.text);
                    Debug.Log("--- Project DATA Updated ---");
                    ToggleCheckErrorWhileDownloading(start: false);
                }
            }
            else
            {
                ShowDownloadSuccessError("Update Project", jo);
            }

            ToggleEscapeFunctionality(true);
            UIManager.instance.ToggleDownloadProgressPanel(false);
        }
        else
        {
            Debug.Log("Project Details Download : Failed\nError : " + downloadProject.error);
            _errorWhileDownloading = true;
        }
        LoadingPanel.instance.HideLoadingPanel();
        downloadProject.Dispose();
        downloadProject = null;
    }

    IEnumerator UpdateSubProjectDataEnumerator(string projectDataString, string subProjectId)
    {
        Debug.Log("> Updating Project Data");
        ProjectDataTemplate projectDataTemplate = JsonConvert.DeserializeObject<ProjectDataTemplate>(projectDataString);

        foreach (var subProjectData in projectDataTemplate.data.version.data)
        {
            if (subProjectId == subProjectData.unique_id)
            {
                DataManager.instance.CalculateNoOfDownloadsForUpdatingSubProject(subProjectData);
                UIManager.instance.ToggleDownloadProgressPanel(true);

                Debug.Log("-> Updating Sub Project Data : " + subProjectData.unique_id);
                _currentSubProjectID_BeingDownload = subProjectData.unique_id;
                yield return _downloadCoroutine.StartNestedCoroutines(DownloadSubProjectDataEnumerator(subProjectData));
            }
        }
    }
    #endregion

    #region Custom_CallBacks
    #endregion
}
