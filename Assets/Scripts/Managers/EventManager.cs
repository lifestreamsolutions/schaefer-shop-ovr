﻿public class EventManager
{
    #region Events

    public delegate void ProjectEvents();

    public static event ProjectEvents ProjectOpened;
    public static event ProjectEvents ProjectClosed;
    public static event ProjectEvents ProjectDownloadSuccessed;
    public static event ProjectEvents ProjectDownloadFailed;
    public static event ProjectEvents CurrentProjectDataLoadedFromLocal;
    public static event ProjectEvents ProjectUpdateAvailable;
    public static event ProjectEvents MoveFromSAToPDComplete;

    #endregion

    #region Invokers
    public static void InvokeProjectOpened()
    {
        var handler = ProjectOpened;
        if (handler != null) handler();
    }

    public static void InvokeProjectDownloadSuccessed()
    {
        var handler = ProjectDownloadSuccessed;
        if (handler != null) handler();
    }

    public static void InvokeProjectDownloadFailed()
    {
        var handler = ProjectDownloadFailed;
        if (handler != null) handler();
    }

    public static void InvokeCurrentProjectDataLoadedFromLocal()
    {
        var handler = CurrentProjectDataLoadedFromLocal;
        if (handler != null) handler();
    }

    public static void InvokeProjectClosed()
    {
        var handler = ProjectClosed;
        if (handler != null) handler();
    }

    public static void InvokeProjectUpdateAvailable()
    {
        var handler = ProjectUpdateAvailable;
        if (handler != null) handler();
    }

    public static void InvokeMoveFromSAToPDComplete()
    {
        var handler = MoveFromSAToPDComplete;
        if (handler != null) handler();
    }
    #endregion
}