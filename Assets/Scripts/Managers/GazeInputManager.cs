﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.Events;

public class GazeInputManager : PointerInputModule
{
    #region Public_Variables
    [Tooltip("Time in seconds for a gaze to click")]
    public float gazeToClickTime = 2f;
    public Transform selectedObject;
    #endregion

    #region Private_Variables
    private PointerEventData _pointerEventData;
    private GameObject _currentLookAtHandler;
    private float _currentLookAtHandlerClickTime;
    #endregion

    #region Events
    public UnityEvent onGazeFoucsEnter;
    public UnityEvent onGazeFoucsExit;
//    private UnityEvent OnGazingEvent;
    #endregion

    #region Unity_CallBacks
    void FixedUpdate()
    {
        ProcessGazeMove();
        ProcessGazeClick();
    }

    #endregion

    #region Private_Methods
    void ProcessGazeMove()
    {
        if (_pointerEventData == null)
        {
            _pointerEventData = new PointerEventData(eventSystem);
        }
        // fake a pointer always being at the center of the screen
        _pointerEventData.position = new Vector2(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2);
        _pointerEventData.delta = Vector2.zero;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        eventSystem.RaycastAll(_pointerEventData, raycastResults);
        _pointerEventData.pointerCurrentRaycast = FindFirstRaycast(raycastResults);
        ProcessMove(_pointerEventData);
    }

    void ProcessGazeClick()
    {
        if (_pointerEventData.pointerEnter != null)
        {
            // if the ui receiver has changed, reset the gaze delay timer
            GameObject handler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(_pointerEventData.pointerEnter);

            if (_currentLookAtHandler != handler)
            {
                if (handler != null)
                {
                    if (onGazeFoucsEnter != null)
                    {
                        onGazeFoucsEnter.Invoke();
                    }
                    //                    ReticleController.Instance.StartReticleAnimation(gazeToClickTime); //, handler);
                }
                else
                {
                    if (onGazeFoucsExit != null)
                    {
                        onGazeFoucsExit.Invoke();
                    }
                    //                    ReticleController.Instance.ResetReticleAnimation();
                }

                _currentLookAtHandler = handler;
                _currentLookAtHandlerClickTime = Time.realtimeSinceStartup + gazeToClickTime;
                if (_currentLookAtHandler != null)
                    selectedObject = _currentLookAtHandler.transform;
            }

            // if we have a handler and it's time to click, do it now
            if (_currentLookAtHandler != null && Time.realtimeSinceStartup > _currentLookAtHandlerClickTime)
            {
                //				Debug.Log ("current handler: " + currentLookAtHandler);
                ExecuteEvents.ExecuteHierarchy(_currentLookAtHandler, _pointerEventData, ExecuteEvents.pointerClickHandler);
                _currentLookAtHandlerClickTime = float.MaxValue;
                if (onGazeFoucsExit != null)
                {
                    onGazeFoucsExit.Invoke();
                }
                //                ReticleController.Instance.ResetReticleAnimation();
            }
        }
        else
        {
            if (selectedObject != null)
            {
                if (onGazeFoucsExit != null)
                {
                    onGazeFoucsExit.Invoke();
                }
                //                ReticleController.Instance.ResetReticleAnimation();
            }
            selectedObject = null;
            _currentLookAtHandler = null;
        }
    }
    #endregion

    #region Public_Methods
    public override void Process()
    {
        return;
        ProcessGazeMove();
        ProcessGazeClick();
    }
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    #endregion
}
