﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    #region Public_Variables

    public GameObject overlayCanvas;
#if OVR
    public GameObject keyboardCanvas;
#endif
    public GameObject homePanel;
    public GameObject infoPanel;
    public GameObject demoProjectsPanel;
    public GameObject projectsPanel;
    public GameObject downloadProjectPanel;
    public GameObject barcodeScanPanel;
    public GameObject downloadProgressPanel;
#if GVR
    public GameObject modeChangeCanvas;
    public GameObject modeChangeBackButton;
#endif
    public UIFadeInOut uiFadeInOut;

    [Space]
    public bool overrideSystemEscapeActionForUI = false;

#if GVR
    [Space]
    public Image vrModeToggleImage;
    public Sprite vrModeImage;
    public Sprite normalModeImage;
#endif
    #endregion

    #region Private_Variables
    AlmostAStack<GameObject> _previousPanels = new AlmostAStack<GameObject>();
    private GameObject _currentPanel;
    private float _fadeInOutTime = 0.25f;
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        AppManager.OnSystemEscapePressed += OnSystemEscapePressed;
        EventManager.ProjectOpened += OnProjectOpened;
        EventManager.ProjectClosed += OnProjectClosed;
    }

    // Use this for initialization
    void Start()
    {
        ToggleDownloadProgressPanel(false);
        ShowAsCurrentPanel(homePanel);
#if GVR
        ToggleModeChangeUI(false);
#endif
    }

    void OnDisable()
    {
        AppManager.OnSystemEscapePressed -= OnSystemEscapePressed;
        EventManager.ProjectOpened -= OnProjectOpened;
        EventManager.ProjectClosed -= OnProjectClosed;
    }
    #endregion

    #region Private_Methods

    void FadeInOut(Action action)
    {
        uiFadeInOut.StartFadeIn(_fadeInOutTime, () =>
        {
            if (action != null)
                action.Invoke();
            uiFadeInOut.StartFadeOut(_fadeInOutTime, null);
        });
    }

    void StackPreviousPanel()
    {
        if (!_currentPanel)
            return;

        if (_previousPanels.Contains(_currentPanel))
        {
            _previousPanels.Remove(_previousPanels.items.FindIndex(x => x.Equals(_currentPanel)));
        }
        _previousPanels.Push(_currentPanel);
    }

    void ShowAsCurrentPanel(GameObject panel)
    {
        FadeInOut(() =>
        {
            StackPreviousPanel();
            if (_currentPanel)
                _currentPanel.SetActive(false);
            _currentPanel = panel;
            _currentPanel.SetActive(true);
        });
    }

    void BackToPreviousPanel()
    {
        if (_previousPanels.items.Count > 0)
        {
            FadeInOut(() =>
            {
                _currentPanel.SetActive(false);
                _currentPanel = _previousPanels.Pop();
                if (_currentPanel)
                    _currentPanel.SetActive(true);
                else
                    ShowAsCurrentPanel(homePanel);
            });
        }
        else
        {
            AskForQuit();
        }
    }

    void AskForQuit()
    {
        string yes = LocalizationManager.instance.GetLocalizedValue(Constants.Yes);
        string no = LocalizationManager.instance.GetLocalizedValue(Constants.No);
        string quit = LocalizationManager.instance.GetLocalizedValue(Constants.Quit);
        string confirmMsg = LocalizationManager.instance.GetLocalizedValue("Confirm Message");
        MessagePopupPanel.instance.ShowMessageBox(MessagePopupType.Confirm, quit, confirmMsg,
            () => { Application.Quit(); }, null, yes, no);
    }

#if GVR
    void ToggleModeChangeUI(bool enable)
    {
        modeChangeCanvas.SetActive(enable);
        //        modeChangeBackButton.SetActive();
    }
#endif
    #endregion

    #region Public_Methods

    public void Back()
    {
        BackToPreviousPanel();
    }

    public void ShowDemoProjectsPanel()
    {
#if UNITY_ANDROID
        if (UniAndroidPermission.IsPermitted(AndroidPermission.WRITE_EXTERNAL_STORAGE))
        {
            ShowAsCurrentPanel(demoProjectsPanel);
        }
        else
        {
            UniAndroidPermission.RequestPremission(AndroidPermission.WRITE_EXTERNAL_STORAGE, () =>
            {
                Debug.Log("Storage Permission Enabled Now.");// not getting this callback so manually check permission in coroutine
            });
            StartCoroutine(CheckStoragePermission(() =>
            {
                AppManager.instance.MoveStreamingAssetsToPersistantData();
                EventManager.MoveFromSAToPDComplete += () =>
                {
                    ShowAsCurrentPanel(demoProjectsPanel);
                };
            }));
        }
#elif UNITY_IOS
            ShowAsCurrentPanel(demoProjectsPanel);
#endif

    }

    public void ShowProjectsPanel()
    {
        ShowAsCurrentPanel(projectsPanel);
    }

    public void ShowDownloadProjectPanel()
    {
        ShowAsCurrentPanel(downloadProjectPanel);
    }

    public void ShowBarcodeScanPanel()
    {
        ShowAsCurrentPanel(barcodeScanPanel);
    }

    public void ToggleKeyboardCanvas(bool enable)
    {
#if OVR
        keyboardCanvas.SetActive(enable);
#endif
    }

    public void ToggleInfoPanel(bool enable)
    {
        if (enable)
            ShowAsCurrentPanel(infoPanel);
        else
            Back();
    }

    public void ToggleDownloadProgressPanel(bool enable)
    {
        downloadProgressPanel.SetActive(enable);
    }

#if GVR
    public void ToggleVRMode()
    {
        AppManager.instance.ToggleVRMode(!AppManager.instance.gvrViewer.VRModeEnabled);
        if (!AppManager.instance.gvrViewer.VRModeEnabled)
            vrModeToggleImage.sprite = vrModeImage;
        else
            vrModeToggleImage.sprite = normalModeImage;
    }

    public void ToggleModesBackButton(bool enable)
    {
        modeChangeBackButton.SetActive(enable);
    }

    public void SetNormalModeForIPad()
    {
        vrModeToggleImage.gameObject.SetActive(false);
        modeChangeBackButton.SetActive(true);
    }
#endif
    #endregion

    #region Coroutines
#if UNITY_ANDROID
    IEnumerator CheckStoragePermission(Action action)
    {
        yield return new WaitForSeconds(0.1f);
        if (UniAndroidPermission.IsPermitted(AndroidPermission.WRITE_EXTERNAL_STORAGE))
        {
            Debug.Log("Storage Permission");
            if (action != null)
                action.Invoke();
        }
    }
#endif
    #endregion

    #region Custom_CallBacks
    void OnSystemEscapePressed()
    {
        if (!overrideSystemEscapeActionForUI)
        {
            if (!AppManager.instance.projectOpened)
                BackToPreviousPanel();
        }
    }

    void OnProjectOpened()
    {
#if GVR
        vrModeToggleImage.sprite = normalModeImage;
        ToggleModeChangeUI(true);
#endif
        overlayCanvas.SetActive(false);
    }

    void OnProjectClosed()
    {
#if GVR
        ToggleModeChangeUI(false);
#endif
        overlayCanvas.SetActive(true);
    }

    #endregion
}